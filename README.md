# VtexOrderFormsPod

[![CI Status](http://img.shields.io/travis/Diego Merks/VtexOrderFormsPod.svg?style=flat)](https://travis-ci.org/Diego Merks/VtexOrderFormsPod)
[![Version](https://img.shields.io/cocoapods/v/VtexOrderFormsPod.svg?style=flat)](http://cocoapods.org/pods/VtexOrderFormsPod)
[![License](https://img.shields.io/cocoapods/l/VtexOrderFormsPod.svg?style=flat)](http://cocoapods.org/pods/VtexOrderFormsPod)
[![Platform](https://img.shields.io/cocoapods/p/VtexOrderFormsPod.svg?style=flat)](http://cocoapods.org/pods/VtexOrderFormsPod)

## Fluxo de checkout

### Fluxos possíveis

Items > User > Address > Sla > Payment > Transaction > Send payment > Callback

User > Items > Address > Sla > Payment > Transaction > Send payment > Callback

User > Address > Items > Sla > Payment > Transaction > Send payment > Callback


### Requerimentos

1 - "Address" deve ser chamado apenas depois de "User";

2 - "Sla" deve ser chamado apenas depois de "Address" e "Items";

3 - "Payment" deve ser chamado apenas depois de "Sla";

4 - "Transaction" deve ser chamado apenas depois de "Payment";

5 - "Send payment" deve ser chamado apenas depois de "Transaction";

6 - "Callback" deve ser chamado apenas depois de "Send payment".


### Login

O cookie de login é necessário em dois casos:

1 - Quando se quer ter acesso detalhado aos endereços e dados do usuário;

2 - Quando se quer adicionar ou alterar um endereço de um usuário que possui cadastro.


### Como saber se um usuário possui cadastro?

Após enviar o email ou dados do usuário, basta verificar se o campo "userId" do order form contém algum dado.


## Library initialization example (with login)

```objc
OrderFormApi *orderFormApi = [[OrderFormApi alloc] initWithAccountName:@"ikesaki" andLoginCookie:@"VtexIdclientAutCookie_ikesaki=eyJhbGciOiJFUzI1NiIsImtpZCI6IkZDREMzRTFDQzBDMTIzRURDQTcwMzU5QzNBRkYxRUVDRTFDRDhDNzUiLCJ0eXAiOiJqd3QifQ.eyJzdWIiOiJtZXJrc0Btb2JmaXJzdC5jb20iLCJhY2NvdW50IjoiaWtlc2FraSIsImF1dGhvcml6YWJsZXMiOlsidnJuOmlhbTppa2VzYWtpOnVzZXJzL21lcmtzQG1vYmZpcnN0LmNvbSJdLCJleHAiOjE1MjMzNjQ1NTIsInVzZXJJZCI6IjMwYjg2NzUxLTEwZTUtNDY2ZC1iOTYwLTkwNTFmYzAzZGZmMSIsImlhdCI6MTUyMzI3ODE0OCwianRpIjoiNDM5MmQ0OTMtZTk1NS00ZWNjLWFjZmQtYWQxMTdiZjg5NDlmIiwiaXNzIjoidG9rZW4tZW1pdHRlciJ9.ubtarBbhlAYQKJiXY5qeyGaR7Hil-MjdisUeH4l6Mq64D7bA8CbN_4UdVEJ-MhmUI6oqlZogU6WAjMgBNgAWDQ;VtexIdclientAutCookie_fad1a824-1361-43bc-a21b-bbb362f3648b=eyJhbGciOiJFUzI1NiIsImtpZCI6IkZDREMzRTFDQzBDMTIzRURDQTcwMzU5QzNBRkYxRUVDRTFDRDhDNzUiLCJ0eXAiOiJqd3QifQ.eyJzdWIiOiJtZXJrc0Btb2JmaXJzdC5jb20iLCJhY2NvdW50IjoiaWtlc2FraSIsImF1dGhvcml6YWJsZXMiOlsidnJuOmlhbTppa2VzYWtpOnVzZXJzL21lcmtzQG1vYmZpcnN0LmNvbSJdLCJleHAiOjE1MjMzNjQ1NTIsInVzZXJJZCI6IjMwYjg2NzUxLTEwZTUtNDY2ZC1iOTYwLTkwNTFmYzAzZGZmMSIsImlhdCI6MTUyMzI3ODE0OCwianRpIjoiNDM5MmQ0OTMtZTk1NS00ZWNjLWFjZmQtYWQxMTdiZjg5NDlmIiwiaXNzIjoidG9rZW4tZW1pdHRlciJ9.ubtarBbhlAYQKJiXY5qeyGaR7Hil-MjdisUeH4l6Mq64D7bA8CbN_4UdVEJ-MhmUI6oqlZogU6WAjMgBNgAWDQ"];
```

## Library initialization example (without login)

```objc
OrderFormApi *orderFormApi = [[OrderFormApi alloc] initWithAccountName:@"ikesaki"];
```

## Get order form example

```objc
[self.orderFormApi getOrderFormWithCompletionHandler:^(OrderForm *orderForm, NSString *error) {
        
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add items example

```objc
NSArray *items = [[NSArray alloc] initWithObjects:[[Item alloc] initWithId:@"2016531" quantity:1 andSeller:@"1"], nil];
[[self orderFormApi] addItems:items toOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
    
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add user info example

```objc
UserProfile *userProfile = [[UserProfile alloc] initWithFirstEmail:@"merks@mobfirst.com" email:@"merks@mobfirst.com" firstName:@"Teste" lastName:@"Teste" document:@"065.709.467-65" phone:@"4199999999" documentType:@"CPF" isCorporate:NO andStateInscription:nil];
[[self orderFormApi] sendUserProfileData:userProfile toOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
    
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add user address example

```objc
UserAddress *address = [[[[self orderForm] shippingData] availableAddresses] objectAtIndex:0];
[[self orderFormApi] sendUserAddress:address toOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
    
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Select SLA example

```objc
LogisticsInfo *logisticsInfo = [[[[self orderForm] shippingData] logisticsInfo] objectAtIndex:0];
Sla *sla = [[logisticsInfo slas] objectAtIndex:10];
DeliveryWindow *deliveryWindow = [[sla availableDeliveryWindows] objectAtIndex:9];

[[self orderFormApi] selectSlaWithId:@"Entrega Agendada" deliveryChannel:@"delivery" andDeliveryWindow:deliveryWindow fromOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
    
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add payment example

```objc
[[self orderFormApi] addPaymentSystem:@"6" withInstallmentCount:1 toOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
        
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add transaction example

```objc
[[self orderFormApi] addTransactionToOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
        
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Add transaction example

```objc
[[self orderFormApi] addTransactionToOrderForm:[self orderForm] withCompletionHandler:^(OrderForm *orderForm, NSString *error) {
        
    if(!error) {
        
        self.orderForm = orderForm;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Send payment example

```objc
PaymentApi *paymentApi = [[PaymentApi alloc] initWithAccountName:@"ikesaki"];
[paymentApi placePaymentIntoOrderForm:[self orderForm] withCompletionHandler:^(NSString *orderGroup, NSString *error) {
    
    if(!error) {
        
        self.orderGroup = orderGroup;
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Gateway callback example

```objc
[[self orderFormApi] doGatewayCallbackWithOrderGroup:[self orderGroup] andCompletionHandler:^(NSString *error) {
        
    if(!error) {
        printf("ok");
    }
    else {
        printf("%s", [error UTF8String]);
    }
}];
```

## Requirements

## Installation

VtexOrderFormsPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'VtexOrderFormsPod'
```

## Author

Diego Merks, merks@mobfirst.com

## License

VtexOrderFormsPod is available under the MIT license. See the LICENSE file for more info.
