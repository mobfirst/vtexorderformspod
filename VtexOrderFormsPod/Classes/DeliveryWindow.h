//
//  DeliveryWindow.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeliveryWindow : NSObject

@property (strong, nonatomic) NSString *startDateUtc;
@property (strong, nonatomic) NSString *endDateUtc;
@property (nonatomic) long price;
@property (nonatomic) long lisPrice;
@property (nonatomic) long tax;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;
- (NSDictionary *) toJsonObject;

@end
