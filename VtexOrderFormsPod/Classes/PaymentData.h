//
//  PaymentData.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentData : NSObject

@property (strong, nonatomic) NSMutableArray *installmentOptions;
@property (strong, nonatomic) NSMutableArray *paymentSystems;
@property (strong, nonatomic) NSMutableArray *payments;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
