//
//  PaymentData.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "PaymentData.h"
#import "CheckoutJsonHelper.h"
#import "InstallmentOption.h"
#import "PaymentSystem.h"
#import "Payment.h"

@implementation PaymentData

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *installmentOptionsData;
        if([CheckoutJsonHelper getJsonData:&installmentOptionsData fromDictionary:jsonDictionary withJsonKey:@"installmentOptions"]) {
            
            NSArray *installmentOptionsArray;
            if([CheckoutJsonHelper getJsonArray:&installmentOptionsArray fromJsonData:installmentOptionsData]) {
                
                self.installmentOptions = [[NSMutableArray alloc] init];
                for(NSDictionary *installmentOptionDictionary in installmentOptionsArray) {
                    
                    NSData *installmentOptionData;
                    if(![CheckoutJsonHelper getJsonData:&installmentOptionData fromDictionary:installmentOptionDictionary]) continue;
                    
                    [self.installmentOptions addObject:[[InstallmentOption alloc] initWithJsonString:[[NSString alloc] initWithData:installmentOptionData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *paymentSystemsData;
        if([CheckoutJsonHelper getJsonData:&paymentSystemsData fromDictionary:jsonDictionary withJsonKey:@"paymentSystems"]) {
            
            NSArray *paymentSystemsArray;
            if([CheckoutJsonHelper getJsonArray:&paymentSystemsArray fromJsonData:paymentSystemsData]) {
                
                self.paymentSystems = [[NSMutableArray alloc] init];
                for(NSDictionary *paymentSystemDictionary in paymentSystemsArray) {
                    
                    NSData *paymentSystemData;
                    if(![CheckoutJsonHelper getJsonData:&paymentSystemData fromDictionary:paymentSystemDictionary]) continue;
                    
                    [self.paymentSystems addObject:[[PaymentSystem alloc] initWithJsonString:[[NSString alloc] initWithData:paymentSystemData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *paymentsData;
        if([CheckoutJsonHelper getJsonData:&paymentsData fromDictionary:jsonDictionary withJsonKey:@"payments"]) {
            
            NSArray *paymentsArray;
            if([CheckoutJsonHelper getJsonArray:&paymentsArray fromJsonData:paymentsData]) {
                
                self.payments = [[NSMutableArray alloc] init];
                for(NSDictionary *paymentsDictionary in paymentsArray) {
                    
                    NSData *paymentData;
                    if(![CheckoutJsonHelper getJsonData:&paymentData fromDictionary:paymentsDictionary]) continue;
                    
                    [self.payments addObject:[[Payment alloc] initWithJsonString:[[NSString alloc] initWithData:paymentData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
