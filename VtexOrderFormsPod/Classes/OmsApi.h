//
//  OmsApi.h
//  CheckoutLibrary
//
//  Created by Diego Merks on 16/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderForm.h"

@interface OmsApi : NSObject

@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *loginCookie;

- (id) initWithAccountName:(NSString *) accountName andLoginCookie:(NSString *) loginCookie;
- (void) getBankInvoiceFromOrderGroup:(NSString *) orderGroup withCompletionHandler:(void (^)(NSString *invoiceUrl, NSString *error)) completionHandler;

@end
