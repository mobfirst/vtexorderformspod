//
//  OrderFormApi.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderForm.h"
#import "UserProfile.h"
#import "DeliveryWindow.h"

@interface OrderFormApi : NSObject

@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *loginCookie;
@property (nonatomic, strong) NSString *appKey;
@property (nonatomic, strong) NSString *appToken;

- (id) initWithAccountName:(NSString *) accountName;
- (id) initWithAccountName:(NSString *) accountName andLoginCookie:(NSString *) loginCookie;
- (void) getOrderFormWithCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) addItems:(NSArray *) items toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) updateItemFromOrderForm:(OrderForm *) orderForm atIndex:(int) index withNewQuantity:(int) quantity andCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) deleteAllItemsFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) clearMessagesFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserEmail:(NSString *) email toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserProfileData:(UserProfile *) userProfile toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserAddress:(UserAddress *) userAddress toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) selectSlaWithId:(NSString *) slaId andDeliveryChannel:(NSString *) deliveryChannel fromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) selectSlaWithId:(NSString *) slaId deliveryChannel:(NSString *) deliveryChannel andDeliveryWindow:(DeliveryWindow *) deliveryWindow fromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendCoupon:(NSString *) coupon toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendGiftCard:(NSString *) giftCard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;

- (void) addPaymentSystem:(NSString *) paymentSystem withInstallmentCount:(int) installmentCount andGiftCard:(NSDictionary*) giftcard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) addPaymentSystemAndPaymentGiftCard:(NSString *) paymentSystem withInstallmentCount:(int) installmentCount andGiftCard:(NSDictionary*) giftcard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;

- (void) addPaymentSystem:(NSString *) paymentSystem value:(long) value toOrderForm:(OrderForm *) orderForm andGiftCard:(NSDictionary*) giftcard withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;


- (void) addTransactionToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) doGatewayCallbackWithOrderGroup:(NSString *) orderGroup andCompletionHandler:(void (^)(NSString *error)) completionHandler;
- (void) addItemsFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserEmailFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserProfileDataFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) sendUserAddressFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) selectSlaFromStorageFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;
- (void) addPaymentSystemFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;


- (void) addShippingDataSimulationOrderForm:(OrderForm *) orderForm postalCode:(NSString*) postalCode andCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler;

@end
