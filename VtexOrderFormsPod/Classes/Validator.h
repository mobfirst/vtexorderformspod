//
//  Validator.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validator : NSObject

@property (strong, nonatomic) NSString *regex;
@property (strong, nonatomic) NSString *mask;
@property (strong, nonatomic) NSString *cardCodeRegex;
@property (strong, nonatomic) NSString *cardCodeMask;
@property (nonatomic) BOOL useCvv;
@property (nonatomic) BOOL useExpirationDate;
@property (nonatomic) BOOL useCardHolderName;
@property (nonatomic) BOOL useBillingAddress;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
