//
//  GiftCard.m
//  John John
//
//  Created by Diego P Navarro on 10/09/2018.
//  Copyright © 2018 Diego P Navarro. All rights reserved.
//

#import "GiftCard.h"

@implementation GiftCard


+(id) setGiftCardFromDictionary:(NSDictionary*) dictionary andPaymentId:(NSString*) paymentId{
    
    GiftCard* giftCard = [GiftCard new];
    giftCard.id = [dictionary objectForKey:@"id"];
    giftCard.redemptionCode = [dictionary objectForKey:@"redemptionCode"];
    giftCard.provider = [dictionary objectForKey:@"provider"];
    giftCard.balance = [[dictionary objectForKey:@"balance"] longValue];
    giftCard.value = [[dictionary objectForKey:@"value"] longValue];
    giftCard.paymentId = paymentId;
    
    return giftCard;
}

-(NSDictionary*) getDicionary{

    return @{
             @"provider" : self.provider,
             @"id" : self.id,
             @"redemptionCode" : self.redemptionCode,
             @"balance" : [NSNumber numberWithLong:self.balance],
             @"value" : [NSNumber numberWithLong:self.value],
             @"inUse" : @YES,
             @"isSpecialCard" : @NO,
             @"paymentSystem" : self.paymentId
             };
}

@end
