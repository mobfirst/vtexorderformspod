//
//  LogisticsInfo.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LogisticsInfo : NSObject

@property (nonatomic) int itemIndex;
@property (strong, nonatomic) NSString *selectedSla;
@property (strong, nonatomic) NSString *selectedDeliveryChannel;
@property (strong, nonatomic) NSString *addressId;
@property (strong, nonatomic) NSMutableArray *slas;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
