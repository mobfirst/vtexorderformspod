//
//  CheckoutJsonHelper.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckoutJsonHelper : NSObject

+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonString:(NSString *) jsonString;
+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary withJsonKey:(NSString *) key;
+ (BOOL) getJsonData:(NSData **) jsonData fromArray:(NSArray *) array;
+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary;
+ (BOOL) getJsonArray:(NSArray **) jsonArray fromJsonData:(NSData *) jsonData;
+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonData:(NSData *) jsonData;

@end
