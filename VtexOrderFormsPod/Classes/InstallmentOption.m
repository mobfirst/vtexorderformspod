//
//  InstallmentOption.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "InstallmentOption.h"
#import "CheckoutJsonHelper.h"
#import "Installment.h"

@implementation InstallmentOption

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.paymentSystem = [jsonDictionary objectForKey:@"paymentSystem"];
        self.value = [[jsonDictionary objectForKey:@"value"] longValue];
        
        NSData *installmentsData;
        if([CheckoutJsonHelper getJsonData:&installmentsData fromDictionary:jsonDictionary withJsonKey:@"installments"]) {
            
            NSArray *installmentsArray;
            if([CheckoutJsonHelper getJsonArray:&installmentsArray fromJsonData:installmentsData]) {
                
                self.installments = [[NSMutableArray alloc] init];
                for(NSDictionary *installmentDictionary in installmentsArray) {
                    
                    NSData *installmentData;
                    if(![CheckoutJsonHelper getJsonData:&installmentData fromDictionary:installmentDictionary]) continue;
                    
                    [self.installments addObject:[[Installment alloc] initWithJsonString:[[NSString alloc] initWithData:installmentData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
