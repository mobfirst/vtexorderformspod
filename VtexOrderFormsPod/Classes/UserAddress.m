//
//  UserAddress.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "UserAddress.h"
#import "CheckoutJsonHelper.h"

@implementation UserAddress

- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference {
    
    if(self = [super init]) {
        
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
    }
    return self;
}

- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference {
    
    if(self = [super init]) {
        
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.addressId = addressId;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
    }
    return self;
}

- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement reference:(NSString *) reference addressName:(NSString *)addressName andUserId:(NSString *)userId {
    
    if(self = [super init]) {
        
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.addressId = addressId;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
        self.addressName = addressName;
        self.userId = userId;
    }
    return self;
}

- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference {
    
    if(self = [super init]) {
        
        self.id = id;
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
    }
    return self;
}

- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference {
    
    if(self = [super init]) {
        
        self.id = id;
        self.addressType = addressType;
        self.receiverName = receiverName;
        self.addressId = addressId;
        self.postalCode = postalCode;
        self.city = city;
        self.state = state;
        self.country = country;
        self.street = street;
        self.number = number;
        self.neighborhood = neighborhood;
        self.complement = complement;
        self.reference = reference;
    }
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.addressType = [jsonDictionary objectForKey:@"addressType"];
        self.receiverName = [jsonDictionary objectForKey:@"receiverName"];
        self.addressId = [jsonDictionary objectForKey:@"addressId"];
        self.postalCode = [jsonDictionary objectForKey:@"postalCode"];
        self.city = [jsonDictionary objectForKey:@"city"];
        self.state = [jsonDictionary objectForKey:@"state"];
        self.country = [jsonDictionary objectForKey:@"country"];
        self.street = [jsonDictionary objectForKey:@"street"];
        self.number = [jsonDictionary objectForKey:@"number"];
        self.neighborhood = [jsonDictionary objectForKey:@"neighborhood"];
        self.complement = [jsonDictionary objectForKey:@"complement"];
        self.reference = [jsonDictionary objectForKey:@"reference"];
        self.addressName = [jsonDictionary objectForKey:@"addressName"];
        self.userId = [jsonDictionary objectForKey:@"userId"];
        self.jsonString = jsonString;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if([self id]) {
        if(![[self id] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self id] forKey:@"id"];
        }
    }
    
    if([self addressType]) {
        if(![[self addressType] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self addressType] forKey:@"addressType"];
        }
    }
    
    if([self receiverName]) {
        if(![[self receiverName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self receiverName] forKey:@"receiverName"];
        }
    }
    
    if([self addressId]) {
        if(![[self addressId] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self addressId] forKey:@"addressId"];
        }
    }
    
    if([self postalCode]) {
        if(![[self postalCode] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self postalCode] forKey:@"postalCode"];
        }
    }
    
    if([self city]) {
        if(![[self city] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self city] forKey:@"city"];
        }
    }
    
    if([self state]) {
        if(![[self state] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self state] forKey:@"state"];
        }
    }
    
    if([self country]) {
        if(![[self country] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self country] forKey:@"country"];
        }
    }
    
    if([self street]) {
        if(![[self street] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self street] forKey:@"street"];
        }
    }
    
    if([self number]) {
        if(![[self number] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self number] forKey:@"number"];
        }
    }
    
    if([self neighborhood]) {
        if(![[self neighborhood] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self neighborhood] forKey:@"neighborhood"];
        }
    }
    
    if([self complement]) {
        if(![[self complement] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self complement] forKey:@"complement"];
        }
    }
    
    if([self reference]) {
        if(![[self reference] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self reference] forKey:@"reference"];
        }
    }
    
    if([self addressName]) {
        if(![[self addressName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self addressName] forKey:@"addressName"];
        }
    }
    
    if([self userId]) {
        if(![[self userId] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self userId] forKey:@"userId"];
        }
    }
    
    return dictionary;
}

@end
