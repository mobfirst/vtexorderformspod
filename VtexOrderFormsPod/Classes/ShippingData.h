//
//  ShippingData.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "UserAddress.h"

@interface ShippingData : NSObject

@property (strong, nonatomic) NSString *attachmentId;
@property (strong, nonatomic) UserAddress *address;
@property (strong, nonatomic) NSMutableArray *selectedAddresses;
@property (strong, nonatomic) NSMutableArray *availableAddresses;
@property (strong, nonatomic) NSMutableArray *logisticsInfo;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
