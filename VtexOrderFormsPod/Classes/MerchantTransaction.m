//
//  MerchantTransaction.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 27/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "MerchantTransaction.h"
#import "CheckoutJsonHelper.h"
#import "Payment.h"

@implementation MerchantTransaction

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *paymentsData;
        if([CheckoutJsonHelper getJsonData:&paymentsData fromDictionary:jsonDictionary withJsonKey:@"payments"]) {
            
            NSArray *paymentsArray;
            if([CheckoutJsonHelper getJsonArray:&paymentsArray fromJsonData:paymentsData]) {
                
                self.payments = [[NSMutableArray alloc] init];
                for(NSDictionary *paymentDictionary in paymentsArray) {
                    
                    NSData *paymentData;
                    if(![CheckoutJsonHelper getJsonData:&paymentData fromDictionary:paymentDictionary]) continue;
                    
                    [self.payments addObject:[[Payment alloc] initWithJsonString:[[NSString alloc] initWithData:paymentData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.transactionId = [jsonDictionary objectForKey:@"transactionId"];
        self.merchantName = [jsonDictionary objectForKey:@"merchantName"];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
