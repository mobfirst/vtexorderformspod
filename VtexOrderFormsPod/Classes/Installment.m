//
//  Installment.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Installment.h"
#import "CheckoutJsonHelper.h"

@implementation Installment

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.count = [[jsonDictionary objectForKey:@"count"] intValue];
        self.hasInterestRate = [[jsonDictionary objectForKey:@"hasInterestRate"] boolValue];
        self.interestRate = [[jsonDictionary objectForKey:@"interestRate"] longValue];
        self.value = [[jsonDictionary objectForKey:@"value"] longValue];
        self.total = [[jsonDictionary objectForKey:@"total"] longValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
