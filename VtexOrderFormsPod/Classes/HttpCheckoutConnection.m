//
//  HttpCheckoutConnection.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "HttpCheckoutConnection.h"

@implementation HttpCheckoutConnection

+ (void) doGetFromUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler {
    
    NSMutableString *urlWithParameters = [[NSMutableString alloc] initWithString:stringUrl];
    BOOL firstParameter = YES;
    
    if(parameters) {
        for(NSString *key in [parameters allKeys]) {
            
            [urlWithParameters appendString:[NSString stringWithFormat:firstParameter ? @"?%@=%@" : @"&%@=%@", key, [parameters objectForKey:key]]];
            firstParameter = NO;
        }
    }
    
    NSURL *url = [NSURL URLWithString:[urlWithParameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"GET"];
    
    for(NSString *key in [headers allKeys]) [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:completionHandler] resume];
}

+ (void) doPostToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler {
    
    NSMutableString *urlWithParameters = [[NSMutableString alloc] initWithString:stringUrl];
    BOOL firstParameter = YES;
    
    if(parameters) {
        for(NSString *key in [parameters allKeys]) {
            
            [urlWithParameters appendString:[NSString stringWithFormat:firstParameter ? @"?%@=%@" : @"&%@=%@", key, [parameters objectForKey:key]]];
            firstParameter = NO;
        }
    }
    
    NSURL *url = [NSURL URLWithString:[urlWithParameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    [request setValue:[NSString stringWithFormat:@"%lu", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    for(NSString *key in [headers allKeys]) [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:completionHandler] resume];
}

+ (void) doPutToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler {
    
    NSMutableString *urlWithParameters = [[NSMutableString alloc] initWithString:stringUrl];
    BOOL firstParameter = YES;
    
    if(parameters) {
        for(NSString *key in [parameters allKeys]) {
            
            [urlWithParameters appendString:[NSString stringWithFormat:firstParameter ? @"?%@=%@" : @"&%@=%@", key, [parameters objectForKey:key]]];
            firstParameter = NO;
        }
    }
    
    NSURL *url = [NSURL URLWithString:[urlWithParameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"PUT"];
    [request setHTTPBody:body];
    [request setValue:[NSString stringWithFormat:@"%lu", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    for(NSString *key in [headers allKeys]) [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:completionHandler] resume];
}

+ (void) doPatchToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler {
    
    NSMutableString *urlWithParameters = [[NSMutableString alloc] initWithString:stringUrl];
    BOOL firstParameter = YES;
    
    if(parameters) {
        for(NSString *key in [parameters allKeys]) {
            
            [urlWithParameters appendString:[NSString stringWithFormat:firstParameter ? @"?%@=%@" : @"&%@=%@", key, [parameters objectForKey:key]]];
            firstParameter = NO;
        }
    }
    
    NSURL *url = [NSURL URLWithString:[urlWithParameters stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody:body];
    [request setValue:[NSString stringWithFormat:@"%lu", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    for(NSString *key in [headers allKeys]) [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:completionHandler] resume];
}

@end
