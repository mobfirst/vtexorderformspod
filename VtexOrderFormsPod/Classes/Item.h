//
//  Item.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject

@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *skuName;
@property (nonatomic) long tax;
@property (nonatomic) long price;
@property (nonatomic) long listPrice;
@property (nonatomic) long sellingPrice;
@property (nonatomic) BOOL isGift;
@property (nonatomic) int quantity;
@property (nonatomic, strong) NSString *seller;
@property (nonatomic, strong) NSString *availability;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithId:(NSString *) id quantity:(int) quantity andSeller:(NSString *) seller;
- (id) initWithJsonString:(NSString *) jsonString;
- (NSDictionary *) toJsonObject;

@end
