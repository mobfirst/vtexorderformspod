//
//  Payment.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Payment.h"
#import "CheckoutJsonHelper.h"

@implementation Payment

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.paymentSystem = [jsonDictionary objectForKey:@"paymentSystem"];
        self.installments = [[jsonDictionary objectForKey:@"installments"] intValue];
        self.value = [[jsonDictionary objectForKey:@"value"] longValue];
        self.referenceValue = [[jsonDictionary objectForKey:@"referenceValue"] longValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
