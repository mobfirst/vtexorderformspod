//
//  HttpCheckoutConnection.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TIMEOUT_INTERVAL 60.0

@interface HttpCheckoutConnection : NSObject

+ (void) doGetFromUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler;
+ (void) doPostToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler;
+ (void) doPutToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler;
+ (void) doPatchToUrl:(NSString *) stringUrl withHeaders:(NSDictionary *) headers parameters:(NSDictionary *) parameters body:(NSData *) body andCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler;

@end
