//
//  CreditCard.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "CreditCard.h"

@implementation CreditCard

- (id) initWithHolderName:(NSString *) holderName cardNumber:(NSString *) cardNumber validationCode:(NSString *) validationCode dueDate:(NSString *) dueDate andAddressId:(NSString *) addressId {
    
    if(self = [super init]) {
        
        self.holderName = holderName;
        self.cardNumber = cardNumber;
        self.validationCode = validationCode;
        self.dueDate = dueDate;
        self.addressId = addressId;
    }
    
    return self;
}

- (id) initWithDictionary:(NSDictionary *) dictionary {
    
    if(self = [super init]) {
        
        self.holderName = [dictionary objectForKey:@"holderName"];
        self.cardNumber = [dictionary objectForKey:@"cardNumber"];
        self.validationCode = [dictionary objectForKey:@"validationCode"];
        self.dueDate = [dictionary objectForKey:@"dueDate"];
        self.addressId = [dictionary objectForKey:@"addressId"];
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if([self holderName]) {
        if(![[self holderName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self holderName] forKey:@"holderName"];
        }
    }
    
    if([self cardNumber]) {
        if(![[self cardNumber] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self cardNumber] forKey:@"cardNumber"];
        }
    }
    
    if([self validationCode]) {
        if(![[self validationCode] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self validationCode] forKey:@"validationCode"];
        }
    }
    
    if([self dueDate]) {
        if(![[self dueDate] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self dueDate] forKey:@"dueDate"];
        }
    }
    
    if([self addressId]) {
        if(![[self addressId] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self addressId] forKey:@"addressId"];
        }
    }
    
    return dictionary;
}

@end
