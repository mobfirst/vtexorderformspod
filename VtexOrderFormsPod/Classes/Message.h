//
//  Message.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
