//
//  FulfillmentApi.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/06/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FulfillmentApi : NSObject

@property (strong, nonatomic) NSString *accountName;
@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) NSString *apiToken;

- (id)initWithAccountName:(NSString *)accountName apiKey:(NSString *)apiKey andApiToken:(NSString *)apiToken;
- (void)simulateShippingWithItems:(NSArray *)items postalCode:(NSString *)postalCode country:(NSString *)country andCompletionHandler:(void (^)(NSArray *logisticsInfo, NSString *error)) completionHandler;

@end
