//
//  UserProfile.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *firstEmail;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *document;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *documentType;
@property (nonatomic) BOOL isCorporate;
@property (strong, nonatomic) NSString *stateInscription;
@property (strong, nonatomic) NSString *attachmentId;
@property (strong, nonatomic) NSString *corporateName;
@property (strong, nonatomic) NSString *tradeName;
@property (strong, nonatomic) NSString *corporateDocument;
@property (strong, nonatomic) NSString *corporatePhone;
@property (nonatomic) BOOL profileCompleteOnLoading;
@property (nonatomic) BOOL profileErrorOnLoading;
@property (strong, nonatomic) NSString *customerClass;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithFirstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate andStateInscription:(NSString *) stateInscription;
- (id) initWithFirstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate stateInscription:(NSString *) stateInscription attachmentId:(NSString *) attachmentId corporateName:(NSString *) corporateName tradeName:(NSString *) tradeName corporateDocument:(NSString *) corporateDocument corporatePhone:(NSString *) corporatePhone profileCompleteOnLoading:(BOOL) profileCompleteOnLoading profileErrorOnLoading:(BOOL) profileErrorOnLoading andCustomerClass:(NSString *) customerClass;
- (id) initWithId:(NSString *) id firstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate andStateInscription:(NSString *) stateInscription;
- (id) initWithId:(NSString *) id firstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate stateInscription:(NSString *) stateInscription attachmentId:(NSString *) attachmentId corporateName:(NSString *) corporateName tradeName:(NSString *) tradeName corporateDocument:(NSString *) corporateDocument corporatePhone:(NSString *) corporatePhone profileCompleteOnLoading:(BOOL) profileCompleteOnLoading profileErrorOnLoading:(BOOL) profileErrorOnLoading andCustomerClass:(NSString *) customerClass;
- (id) initWithJsonString:(NSString *) jsonString;
- (NSDictionary *) toJsonObject;

@end
