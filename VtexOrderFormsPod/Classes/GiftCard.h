//
//  GiftCard.h
//  John John
//
//  Created by Diego P Navarro on 10/09/2018.
//  Copyright © 2018 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftCard : NSObject

@property (strong,nonatomic) NSString* id;
@property (strong,nonatomic) NSString* redemptionCode;
@property (strong,nonatomic) NSString* provider;
@property (nonatomic) long balance;
@property (nonatomic) long value;
@property(nonatomic,strong) NSString* paymentId;

+(id) setGiftCardFromDictionary:(NSDictionary*) dictionary andPaymentId:(NSString*) paymentId;

-(NSDictionary*) getDicionary;

@end
