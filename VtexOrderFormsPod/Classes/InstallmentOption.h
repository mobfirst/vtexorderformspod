//
//  InstallmentOption.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstallmentOption : NSObject

@property (strong, nonatomic) NSString *paymentSystem;
@property (nonatomic) long value;
@property (strong, nonatomic) NSMutableArray *installments;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
