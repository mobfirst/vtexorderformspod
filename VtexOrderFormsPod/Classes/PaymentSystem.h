//
//  PaymentSystem.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Validator.h"

@interface PaymentSystem : NSObject

@property (nonatomic) int id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *groupName;
@property (strong, nonatomic) Validator *validator;
@property (strong, nonatomic) NSString *stringId;
@property (strong, nonatomic) NSString *template;
@property (nonatomic) BOOL isCustom;
@property (nonatomic) BOOL requiresAuthentication;
@property (strong, nonatomic) NSString *dueDate;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
