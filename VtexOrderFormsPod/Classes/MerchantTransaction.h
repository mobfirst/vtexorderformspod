//
//  MerchantTransaction.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 27/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MerchantTransaction : NSObject

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *transactionId;
@property (strong, nonatomic) NSString *merchantName;
@property (strong, nonatomic) NSMutableArray *payments;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
