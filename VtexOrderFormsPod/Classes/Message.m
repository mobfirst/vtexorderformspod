//
//  Message.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Message.h"
#import "CheckoutJsonHelper.h"

@implementation Message

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.code = [jsonDictionary objectForKey:@"code"];
        self.text = [jsonDictionary objectForKey:@"text"];
        self.status = [jsonDictionary objectForKey:@"status"];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
