//
//  LogisticsInfo.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "LogisticsInfo.h"
#import "CheckoutJsonHelper.h"
#import "Sla.h"

@implementation LogisticsInfo

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *slasData;
        if([CheckoutJsonHelper getJsonData:&slasData fromDictionary:jsonDictionary withJsonKey:@"slas"]) {
            
            NSArray *slasArray;
            if([CheckoutJsonHelper getJsonArray:&slasArray fromJsonData:slasData]) {
                
                self.slas = [[NSMutableArray alloc] init];
                for(NSDictionary *slaDictionary in slasArray) {
                    
                    NSData *slaData;
                    if(![CheckoutJsonHelper getJsonData:&slaData fromDictionary:slaDictionary]) continue;
                    
                    [self.slas addObject:[[Sla alloc] initWithJsonString:[[NSString alloc] initWithData:slaData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        self.itemIndex = [[jsonDictionary objectForKey:@"itemIndex"] intValue];
        self.selectedSla = [jsonDictionary objectForKey:@"selectedSla"];
        self.selectedDeliveryChannel = [jsonDictionary objectForKey:@"selectedDeliveryChannel"];
        self.addressId = [jsonDictionary objectForKey:@"addressId"];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
