//
//  OrderForm.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "UserAddress.h"
#import "ShippingData.h"
#import "PaymentData.h"
#import "MerchantTransaction.h"

@interface OrderForm : NSObject

@property (strong, nonatomic) NSString *orderFormId;
@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSString *userProfileId;
@property (strong, nonatomic) UserProfile *userProfile;
@property (strong, nonatomic) ShippingData *shippingData;
@property (strong, nonatomic) NSMutableArray *totalizers;
@property (strong, nonatomic) NSMutableArray *merchantTransactions;
@property (strong, nonatomic) PaymentData *paymentData;
@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *gatewayCallbackTemplatePath;
@property (strong, nonatomic) NSString *orderId;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
