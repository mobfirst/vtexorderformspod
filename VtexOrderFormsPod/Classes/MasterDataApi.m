//
//  MasterdataApi.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 25/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "MasterDataApi.h"
#import "HttpCheckoutConnection.h"
#import "CheckoutJsonHelper.h"

@implementation MasterDataApi

- (id) initWithAccountName:(NSString *) accountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken {
    
    if(self = [super init]) {
        
        self.accountName = accountName;
        self.vtexApiKey = vtexApiKey;
        self.vtexApiToken = vtexApiToken;
    }
    
    return self;
}

- (void) getUserProfileWithUserId:(NSString *) userId andCompletionHandler:(void (^)(UserProfile *userProfile, NSString *error)) completionHandler {
    
    if(userId) {
        
        NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"application/json", @"accept",
                                 @"application/json", @"content-type",
                                 [self vtexApiKey], @"X-VTEX-API-AppKey",
                                 [self vtexApiToken], @"X-VTEX-API-AppToken", nil];
        
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    userId, @"userId",
                                    @"id,email,firstName,lastName,documentType,document,phone", @"_fields", nil];
        
        NSString *url = [NSString stringWithFormat:@"http://api.vtexcrm.com.br/%@/dataentities/CL/search", [self accountName]];
        
        [HttpCheckoutConnection doGetFromUrl:url withHeaders:headers parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    UserProfile *userProfile = [[UserProfile alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(userProfile) completionHandler(userProfile, nil);
                    else completionHandler(nil, @"error parsing response");
                }
                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(nil, @"http error");
        }];
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) updateUserProfile:(UserProfile *) oldUserProfile withNewUserProfile:(UserProfile *) newUserProfile andCompletionHandler:(void (^)(NSString *error)) completionHandler {
    
    if(oldUserProfile && newUserProfile) {
        
        NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"application/json", @"accept",
                                 @"application/json", @"content-type",
                                 [self vtexApiKey], @"X-VTEX-API-AppKey",
                                 [self vtexApiToken], @"X-VTEX-API-AppToken", nil];
        
        NSString *url = [NSString stringWithFormat:@"http://api.vtexcrm.com.br/%@/dataentities/CL/documents/%@", [self accountName], [oldUserProfile id]];
        
        NSData *bodyData;
        NSDictionary *bodyDictionary = [newUserProfile toJsonObject];
        
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            [HttpCheckoutConnection doPatchToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    
                    if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(nil);
                    else completionHandler([[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(@"http error");
            }];
        }
        else completionHandler(@"error creating json object");
    }
    else completionHandler(@"parameters cannot be null");
}

- (void) getUserAddressesFromUserProfile:(UserProfile *) userProfile andCompletionHandler:(void (^)(NSArray *addresses, NSString *error)) completionHandler {
    
    if(userProfile) {
        
        NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"application/json", @"accept",
                                 @"application/json", @"content-type",
                                 [self vtexApiKey], @"X-VTEX-API-AppKey",
                                 [self vtexApiToken], @"X-VTEX-API-AppToken", nil];
        
        NSString *url = [NSString stringWithFormat:@"http://api.vtexcrm.com.br/%@/dataentities/AD/search", [self accountName]];
        
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    [userProfile id], @"userId",
                                    @"id,street,number,neighborhood,postalCode,city,state,country,addressType,addressName,receiverName,complement,reference", @"_fields", nil];
        
        [HttpCheckoutConnection doGetFromUrl:url withHeaders:headers parameters:parameters andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if(jsonString) {
                        
                        NSDictionary *jsonDictionary;
                        if([CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) {
                            
                            NSData *addressesData;
                            if([CheckoutJsonHelper getJsonData:&addressesData fromDictionary:jsonDictionary]) {
                                
                                NSArray *addressesArray;
                                if([CheckoutJsonHelper getJsonArray:&addressesArray fromJsonData:addressesData]) {
                                    
                                    NSMutableArray *addresses = [[NSMutableArray alloc] init];
                                    for(NSDictionary *address in addressesArray) {
                                        
                                        NSData *addressData;
                                        if(![CheckoutJsonHelper getJsonData:&addressData fromDictionary:address]) continue;
                                        
                                        [addresses addObject:[[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:addressData encoding:NSUTF8StringEncoding]]];
                                    }
                                    
                                    completionHandler(addresses, nil);
                                }
                                else completionHandler(nil, @"error parsing response");
                            }
                            else completionHandler(nil, @"error parsing response");
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, @"error parsing response");
                }
                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(nil, @"http error");
        }];
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) updateUserAddress:(UserAddress *) oldUserAddress withNewUserAddress:(UserAddress *) newUserAddress andCompletionHandler:(void (^)(NSString *error)) completionHandler {
    
    if(oldUserAddress && newUserAddress) {
        
        NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"application/json", @"accept",
                                 @"application/json", @"content-type",
                                 [self vtexApiKey], @"X-VTEX-API-AppKey",
                                 [self vtexApiToken], @"X-VTEX-API-AppToken", nil];
        
        NSString *url = [NSString stringWithFormat:@"http://api.vtexcrm.com.br/%@/dataentities/AD/documents/%@", [self accountName], [oldUserAddress id]];
        
        NSData *bodyData;
        NSDictionary *bodyDictionary = [newUserAddress toJsonObject];
        
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPatchToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(nil);
                    else completionHandler([[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(@"http error");
            }];
        }
        else completionHandler(@"error creating json object");
    }
    else completionHandler(@"parameters cannot be null");
}

- (void) createUserAddressWithUserProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(NSString *error)) completionHandler {
    
    if(userProfile && userAddress) {
        if([userProfile id]) {
            
            NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"application/json", @"accept",
                                     @"application/json", @"content-type",
                                     [self vtexApiKey], @"X-VTEX-API-AppKey",
                                     [self vtexApiToken], @"X-VTEX-API-AppToken", nil];
            
            NSString *url = [NSString stringWithFormat:@"http://api.vtexcrm.com.br/%@/dataentities/AD/documents", [self accountName]];
            
            NSData *bodyData;
            NSDictionary *bodyDictionary = [userAddress toJsonObject];
            
            if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                
                [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response && data) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 201) completionHandler(nil);
                        else completionHandler([[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    }
                    else completionHandler(@"http error");
                }];
            }
            else completionHandler(@"error creating json object");
        }
        else completionHandler(@"parameters cannot be null");
    }
    else completionHandler(@"parameters cannot be null");
}

@end
