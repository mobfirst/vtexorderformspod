//
//  PaymentSystem.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "PaymentSystem.h"
#import "CheckoutJsonHelper.h"

@implementation PaymentSystem

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [[jsonDictionary objectForKey:@"id"] intValue];
        self.name = [jsonDictionary objectForKey:@"name"];
        self.groupName = [jsonDictionary objectForKey:@"groupName"];
        self.stringId = [jsonDictionary objectForKey:@"stringId"];
        self.template = [jsonDictionary objectForKey:@"template"];
        self.dueDate = [jsonDictionary objectForKey:@"dueDate"];
        self.isCustom = [[jsonDictionary objectForKey:@"isCustom"] boolValue];
        self.requiresAuthentication = [[jsonDictionary objectForKey:@"requiresAuthentication"] boolValue];
        
        NSData *validatorData;
        if([CheckoutJsonHelper getJsonData:&validatorData fromDictionary:jsonDictionary withJsonKey:@"validator"]) {
            self.validator = [[Validator alloc] initWithJsonString:[[NSString alloc] initWithData:validatorData encoding:NSUTF8StringEncoding]];
        }
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
