//
//  OrderFormCheckoutStorage.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 09/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "UserAddress.h"
#import "DeliveryWindow.h"

@interface OrderFormCheckoutStorage : NSObject

+ (BOOL) getItems:(NSArray **) items;
+ (BOOL) getUserEmail:(NSString **) email;
+ (BOOL) getUserProfileData:(UserProfile **) userProfile;
+ (BOOL) getUserAddress:(UserAddress **) userAddress;
+ (BOOL) getSlaId:(NSString **) slaId;
+ (BOOL) getDeliveryChannel:(NSString **) deliveryChannel;
+ (BOOL) getDeliveryWindow:(DeliveryWindow **) deliveryWindow;
+ (BOOL) getPaymentSystem:(NSString **) paymentSystem;
+ (BOOL) getInstallmentCount:(int *) installmentCount;
+ (BOOL) saveItems:(NSArray *) items;
+ (BOOL) saveUserEmail:(NSString *) email;
+ (BOOL) saveUserProfileData:(UserProfile *) userProfile;
+ (BOOL) saveUserAddress:(UserAddress *) userAddress;
+ (BOOL) saveSlaId:(NSString *) slaId;
+ (BOOL) saveDeliveryChannel:(NSString *) deliveryChannel;
+ (BOOL) saveDeliveryWindow:(DeliveryWindow *) deliveryWindow;
+ (BOOL) savePaymentSystem:(NSString *) paymentSystem;
+ (BOOL) saveInstallmentCount:(int) installmentCount;
+ (BOOL) deleteDeliveryWindow;

@end
