//
//  OrderFormApi.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "OrderFormApi.h"
#import "HttpCheckoutConnection.h"
#import "Item.h"
#import "CheckoutJsonHelper.h"
#import "ShippingData.h"
#import "LogisticsInfo.h"
#import "Sla.h"
#import "UserAddress.h"
#import "InstallmentOption.h"
#import "Installment.h"
#import "Payment.h"
#import "OrderFormCheckoutStorage.h"

@implementation OrderFormApi

- (id) initWithAccountName:(NSString *) accountName {
    
    if(self = [super init]) {
        self.accountName = accountName;
    }
    
    return self;
}

- (id) initWithAccountName:(NSString *) accountName andLoginCookie:(NSString *) loginCookie {
    
    if(self = [super init]) {
        
        self.accountName = accountName;
        self.loginCookie = loginCookie;
    }
    
    return self;
}


- (void) getOrderFormWithCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm", [self accountName]];
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                    @"application/json", @"accept",
                                    @"application/json", @"content-type", nil];
    
    if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
    
    [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:nil andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!error && response && data) {
            if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                
                OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                
                if(orderForm){
                    [self setOrderFormMarketingData:orderForm WithCompletionHandler:completionHandler];
                }
                else completionHandler(nil, @"error parsing response");
            }
            else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
        else completionHandler(nil, @"http error");
    }];
}


- (void) setOrderFormMarketingData:(OrderForm*) orderForm WithCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/marketingData", [self accountName],orderForm.orderFormId];
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                    @"application/json", @"accept",
                                    @"application/json", @"content-type",
                                    self.appKey, @"X-VTEX-API-AppKey",
                                    self.appToken ,@"X-VTEX-API-AppToken",
                                    nil];
    
    if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
    
    NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    @"app_ios", @"utmSource", nil];
    
    
    NSData *bodyData;
    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
        
        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(orderForm) completionHandler(orderForm, nil);
                    else completionHandler(nil, @"error parsing response");
                }
                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(nil, @"http error");
        }];
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) addShippingDataSimulationOrderForm:(OrderForm *) orderForm postalCode:(NSString*) postalCode andCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler{
    
    NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/shippingData", [self accountName],orderForm.orderFormId];
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                    @"application/json", @"accept",
                                    @"application/json", @"content-type",
                                    self.appKey, @"X-VTEX-API-AppKey",
                                    self.appToken ,@"X-VTEX-API-AppToken",
                                    nil];
    
    NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                          @"items",
                                          @"totalizers",
                                          @"clientProfileData",
                                          @"shippingData",
                                          @"paymentData",
                                          @"sellers",
                                          @"messages",
                                          @"marketingData",
                                          @"clientPreferencesData",
                                          @"storePreferencesData",
                                          @"giftRegistryData",
                                          @"ratesAndBenefitsData",
                                          @"openTextField",
                                          @"commercialConditionData",
                                          @"customData", nil];
    
    if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
    
    
    
    NSDictionary *dictionaryAddress = [[NSDictionary alloc] initWithObjectsAndKeys:
                                       @"BRA", @"country",
                                       postalCode,@"postalCode", nil];
    
    
    NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    dictionaryAddress, @"address",
                                    expectedOrderFormSections, @"expectedOrderFormSections", nil];
    
    NSData *bodyData;
    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
        
        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                    
                    if(orderForm) completionHandler(orderForm, nil);
                    else completionHandler(nil, @"error parsing response");
                }
                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(nil, @"http error");
        }];
    }
    else completionHandler(nil, @"parameters cannot be null");
}


- (void) addItems:(NSArray *) items toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(items && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/items", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *orderItems = [[NSMutableArray alloc] init];
        
        for(Item *item in items) {
            [orderItems addObject:[item toJsonObject]];
        }
        
        NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        orderItems, @"orderItems",
                                        expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        if(orderForm) {
                            
                            [OrderFormCheckoutStorage saveItems:[orderForm items]];
                            completionHandler(orderForm, nil);
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) updateItemFromOrderForm:(OrderForm *) orderForm atIndex:(int) index withNewQuantity:(int) quantity andCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/items/update", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *orderItems = [[NSMutableArray alloc] init];
        if([[orderForm items] count] > index) {
            
            Item *item = [[orderForm items] objectAtIndex:index];
            NSMutableDictionary *itemDictionary = [[NSMutableDictionary alloc] initWithDictionary:[item toJsonObject]];
            
            [itemDictionary setObject:[NSNumber numberWithInteger:index] forKey:@"index"];
            [itemDictionary removeObjectForKey:@"quantity"];
            [itemDictionary setObject:[NSNumber numberWithInteger:quantity] forKey:@"quantity"];
            
            [orderItems addObject:itemDictionary];
            
            NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            orderItems, @"orderItems",
                                            expectedOrderFormSections, @"expectedOrderFormSections", nil];
            
            NSData *bodyData;
            if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                
                [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response && data) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                            
                            OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                            
                            if(orderForm) {
                                
                                [OrderFormCheckoutStorage saveItems:[orderForm items]];
                                completionHandler(orderForm, nil);
                            }
                            else completionHandler(nil, @"error parsing response");
                        }
                        else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    }
                    else completionHandler(nil, @"http error");
                }];
            }
            else completionHandler(nil, @"error creating json object");
        }
        else completionHandler(nil, @"the order form has no items");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) deleteAllItemsFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(orderForm) {
        
        if([orderForm items]) {
            if([[orderForm items] count] > 0) {
                
                NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/items/update", [self accountName], [orderForm orderFormId]];
                NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                @"application/json", @"accept",
                                                @"application/json", @"content-type", nil];
                
                if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
                
                NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                                      @"items",
                                                      @"totalizers",
                                                      @"clientProfileData",
                                                      @"shippingData",
                                                      @"paymentData",
                                                      @"sellers",
                                                      @"messages",
                                                      @"marketingData",
                                                      @"clientPreferencesData",
                                                      @"storePreferencesData",
                                                      @"giftRegistryData",
                                                      @"ratesAndBenefitsData",
                                                      @"openTextField",
                                                      @"commercialConditionData",
                                                      @"customData", nil];
                
                NSArray *items = [orderForm items];
                NSMutableArray *orderItems = [[NSMutableArray alloc] init];
                
                for(int i = 0; i < [items count]; i++) {
                    
                    Item *item = [items objectAtIndex:i];
                    NSMutableDictionary *itemDictionary = [[NSMutableDictionary alloc] initWithDictionary:[item toJsonObject]];
                    
                    [itemDictionary setObject:[NSNumber numberWithInteger:i] forKey:@"index"];
                    [itemDictionary removeObjectForKey:@"quantity"];
                    [itemDictionary setObject:[NSNumber numberWithInteger:0] forKey:@"quantity"];
                    
                    [orderItems addObject:itemDictionary];
                }
                
                NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                orderItems, @"orderItems",
                                                expectedOrderFormSections, @"expectedOrderFormSections", nil];
                
                NSData *bodyData;
                if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                    
                    [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        
                        if(!error && response && data) {
                            if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                
                                OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                
                                if(orderForm) {
                                    
                                    [OrderFormCheckoutStorage saveItems:[orderForm items]];
                                    completionHandler(orderForm, nil);
                                }
                                else completionHandler(nil, @"error parsing response");
                            }
                            else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                        }
                        else completionHandler(nil, @"http error");
                    }];
                }
                else completionHandler(nil, @"error creating json object");
            }
            else completionHandler(nil, @"the order form has no items");
        }
        else completionHandler(nil, @"the order form has no items");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) clearMessagesFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/messages/clear", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        
                        if(orderForm) completionHandler(orderForm, nil);
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) sendUserEmail:(NSString *) email toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(email && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/clientProfileData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        email, @"email",
                                        expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        if(orderForm) {
                            
                            [OrderFormCheckoutStorage saveUserEmail:email];
                            completionHandler(orderForm, nil);
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) sendUserProfileData:(UserProfile *) userProfile toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(userProfile && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/clientProfileData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        if([userProfile firstEmail]) [bodyDictionary setObject:[userProfile firstEmail] forKey:@"firstEmail"];
        if([userProfile email]) [bodyDictionary setObject:[userProfile email] forKey:@"email"];
        if([userProfile firstName]) [bodyDictionary setObject:[userProfile firstName] forKey:@"firstName"];
        if([userProfile lastName]) [bodyDictionary setObject:[userProfile lastName] forKey:@"lastName"];
        if([userProfile document]) [bodyDictionary setObject:[userProfile document] forKey:@"document"];
        if([userProfile phone]) [bodyDictionary setObject:[userProfile phone] forKey:@"phone"];
        if([userProfile documentType]) [bodyDictionary setObject:[userProfile documentType] forKey:@"documentType"];
        if([userProfile isCorporate]) [bodyDictionary setObject:[NSNumber numberWithBool:[userProfile isCorporate]] forKey:@"isCorporate"];
        if([userProfile stateInscription]) [bodyDictionary setObject:[userProfile stateInscription] forKey:@"stateInscription"];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        
                        if(orderForm) {
                            
                            [OrderFormCheckoutStorage saveUserProfileData:[orderForm userProfile]];
                            completionHandler(orderForm, nil);
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) sendUserAddress:(UserAddress *) userAddress toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(userAddress && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/shippingData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               [userAddress toJsonObject], @"address",
                                               expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        if(orderForm) {
                            
                            if([orderForm shippingData]) {
                                
                                ShippingData *shippingData = [orderForm shippingData];
                                if(shippingData) {
                                    
                                    UserAddress *address = [shippingData address];
                                    [OrderFormCheckoutStorage saveUserAddress:address];
                                }
                            }
                            
                            completionHandler(orderForm, nil);
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) selectSlaWithId:(NSString *) slaId andDeliveryChannel:(NSString *) deliveryChannel fromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(slaId && deliveryChannel && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/shippingData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *logisticsInfo = [[NSMutableArray alloc] init];
        if([orderForm shippingData]) {
            
            ShippingData *shippingData = [orderForm shippingData];
            if([shippingData logisticsInfo] && [shippingData address]) {
                
                NSArray *logisticsInfoArray = [shippingData logisticsInfo];
                for(LogisticsInfo *logisticsItem in logisticsInfoArray) {
                    
                    if([logisticsItem slas]) {
                        
                        NSArray *slas = [logisticsItem slas];
                        for(Sla *sla in slas) {
                            
                            if([[sla id] isEqualToString:slaId] && [[sla deliveryChannel] isEqualToString:deliveryChannel]) {
                                
                                NSDictionary *logisticsItemDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                         [NSNumber numberWithInt:[logisticsItem itemIndex]], @"itemIndex",
                                                                         [sla id], @"selectedSla",
                                                                         [sla deliveryChannel], @"selectedDeliveryChannel", nil];
                                
                                [logisticsInfo addObject:logisticsItemDictionary];
                            }
                        }
                    }
                }
                
                if([logisticsInfo count] == [logisticsInfoArray count]) {
                    
                    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                           logisticsInfo, @"logisticsInfo",
                                                           [[shippingData address] toJsonObject], @"address",
                                                           expectedOrderFormSections, @"expectedOrderFormSections", nil];
                    
                    NSData *bodyData;
                    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response && data) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                    
                                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    if(orderForm) {
                                        
                                        [OrderFormCheckoutStorage saveSlaId:slaId];
                                        [OrderFormCheckoutStorage saveDeliveryChannel:deliveryChannel];
                                        [OrderFormCheckoutStorage deleteDeliveryWindow];
                                        
                                        completionHandler(orderForm, nil);
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                            }
                            else completionHandler(nil, @"http error");
                        }];
                    }
                    else completionHandler(nil, @"error creating json object");
                }
                else completionHandler(nil, @"the desired delivery method is not available to all items");
            }
            else completionHandler(nil, @"order form missing logistics information");
        }
        else completionHandler(nil, @"order form missing shipping information");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) selectSlaWithId:(NSString *) slaId deliveryChannel:(NSString *) deliveryChannel andDeliveryWindow:(DeliveryWindow *) deliveryWindow fromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(slaId && deliveryChannel && deliveryWindow && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/shippingData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *logisticsInfo = [[NSMutableArray alloc] init];
        if([orderForm shippingData]) {
            
            ShippingData *shippingData = [orderForm shippingData];
            if([shippingData logisticsInfo] && [shippingData address]) {
                
                NSArray *logisticsInfoArray = [shippingData logisticsInfo];
                for(LogisticsInfo *logisticsItem in logisticsInfoArray) {
                    
                    if([logisticsItem slas]) {
                        
                        NSArray *slas = [logisticsItem slas];
                        for(Sla *sla in slas) {
                            
                            if([sla id] && [sla deliveryChannel]) {
                                
                                if([[sla id] isEqualToString:slaId] && [[sla deliveryChannel] isEqualToString:deliveryChannel]) {
                                    if([sla availableDeliveryWindows]) {
                                        
                                        NSArray *availableDeliveryWindows = [sla availableDeliveryWindows];
                                        for(DeliveryWindow *deliveryWindowItem in availableDeliveryWindows) {
                                            
                                            if([deliveryWindowItem startDateUtc] && [deliveryWindowItem endDateUtc]) {
                                                
                                                if([[deliveryWindowItem startDateUtc] isEqualToString:[deliveryWindow startDateUtc]] && [[deliveryWindowItem endDateUtc] isEqualToString:[deliveryWindow endDateUtc]]) {
                                                    
                                                    
                                                    NSDictionary *deliveryWindowDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                                              [deliveryWindowItem startDateUtc], @"startDateUtc",
                                                                                              [deliveryWindowItem endDateUtc], @"endDateUtc",
                                                                                              [NSNumber numberWithLong:[deliveryWindowItem price]], @"price",
                                                                                              [NSNumber numberWithLong:[deliveryWindowItem lisPrice]], @"lisPrice",
                                                                                              [NSNumber numberWithLong:[deliveryWindowItem tax]], @"tax",
                                                                                              [NSNumber numberWithBool:YES], @"isWindowSelected", nil];
                                                    
                                                    NSDictionary *logisticsItemDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                                             [NSNumber numberWithInt:[logisticsItem itemIndex]], @"itemIndex",
                                                                                             [sla id], @"selectedSla",
                                                                                             [sla deliveryChannel], @"selectedDeliveryChannel",
                                                                                             deliveryWindowDictionary, @"deliveryWindow", nil];
                                                    
                                                    [logisticsInfo addObject:logisticsItemDictionary];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if([logisticsInfo count] == [logisticsInfoArray count]) {
                    
                    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                           logisticsInfo, @"logisticsInfo",
                                                           [[shippingData address] toJsonObject], @"address",
                                                           expectedOrderFormSections, @"expectedOrderFormSections", nil];
                    
                    NSData *bodyData;
                    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response && data) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                    
                                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    if(orderForm) {
                                        
                                        [OrderFormCheckoutStorage saveSlaId:slaId];
                                        [OrderFormCheckoutStorage saveDeliveryChannel:deliveryChannel];
                                        [OrderFormCheckoutStorage saveDeliveryWindow:deliveryWindow];
                                        
                                        completionHandler(orderForm, nil);
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                            }
                            else completionHandler(nil, @"http error");
                        }];
                    }
                    else completionHandler(nil, @"error creating json object");
                }
                else completionHandler(nil, @"the desired delivery method is not available to all items");
            }
            else completionHandler(nil, @"order form missing logistics information");
        }
        else completionHandler(nil, @"order form missing shipping information");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) sendCoupon:(NSString *) coupon toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(coupon && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/coupons", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               coupon, @"text",
                                               expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        
                        if(orderForm) completionHandler(orderForm, nil);
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) sendGiftCard:(NSString *) giftCard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(giftCard && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/paymentData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSDictionary *giftCardDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            giftCard, @"redemptionCode",
                                            [NSNumber numberWithBool:YES], @"inUse", nil];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               [[NSArray alloc] initWithObjects:giftCardDictionary, nil], @"giftCards",
                                               expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                        
                        if(orderForm) completionHandler(orderForm, nil);
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) addPaymentSystem:(NSString *) paymentSystem value:(long) value toOrderForm:(OrderForm *) orderForm andGiftCard:(NSDictionary*) giftcard withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler{
    
    if(paymentSystem && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/paymentData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *payments = [[NSMutableArray alloc] init];
        
        
        NSDictionary *paymentDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                           paymentSystem, @"paymentSystem",
                                           @"", @"installments",
                                           [NSNumber numberWithLong:value], @"referenceValue",
                                           [NSNumber numberWithLong:value], @"value", nil];
        
        [payments addObject:paymentDictionary];
        
        NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                               payments, @"payments",
                                               expectedOrderFormSections, @"expectedOrderFormSections", nil];
        
        if(giftcard){
            NSArray* giftCards = [[NSArray alloc] initWithObjects:giftcard, nil];
            [bodyDictionary addEntriesFromDictionary:@{@"giftCards": giftCards}];
        }
        
        NSData *bodyData;
        if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
            
            [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(!error && response && data) {
                    if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                        
                        completionHandler( [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]], nil);
                    }
                    else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                }
                else completionHandler(nil, @"http error");
            }];
        }
        else completionHandler(nil, @"error creating json object");
    }
    else completionHandler(nil, @"order form missing installment information");
}

- (void) addPaymentSystemAndPaymentGiftCard:(NSString *) paymentSystem withInstallmentCount:(int) installmentCount andGiftCard:(NSDictionary*) giftcard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler{
    
    if(paymentSystem && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/paymentData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *payments = [[NSMutableArray alloc] init];
        if([orderForm paymentData]) {
            
            PaymentData *paymentData = [orderForm paymentData];
            if([paymentData installmentOptions]) {
                
                NSArray *installmentOptions = [paymentData installmentOptions];
                for(InstallmentOption *installmentOption in installmentOptions) {
                    
                    if([installmentOption paymentSystem]) {
                        if([[installmentOption paymentSystem] isEqualToString:paymentSystem]) {
                            
                            if([installmentOption installments]) {
                                
                                NSArray *installments = [installmentOption installments];
                                for(Installment *installment in installments) {
                                    
                                    if([installment count]) {
                                        if([installment count] == installmentCount) {
                                            
                                            NSDictionary *paymentDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                               [installmentOption paymentSystem], @"paymentSystem",
                                                                               [NSNumber numberWithInt:[installment count]], @"installments",
                                                                               [NSNumber numberWithLong:[installment value]], @"referenceValue",
                                                                               [NSNumber numberWithLong:[installment value]], @"value", nil];
                                            
                                            [payments addObject:paymentDictionary];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if([payments count] == 1) {
                    
                    if(giftcard){
                        
                        [payments addObject:@{
                                              @"paymentSystem" : [giftcard objectForKey:@"paymentSystem"],
                                              @"installments" : @1,
                                              @"referenceValue" : [giftcard objectForKey:@"value"],
                                              @"value" : [giftcard objectForKey:@"value"]
                                              }];
                        
                    }
                    
                    
                    
                    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                           payments, @"payments",
                                                           expectedOrderFormSections, @"expectedOrderFormSections", nil];
                    
                    
                    NSData *bodyData;
                    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response && data) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                    
                                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    
                                    if(orderForm) {
                                        
                                        [OrderFormCheckoutStorage savePaymentSystem:paymentSystem];
                                        [OrderFormCheckoutStorage saveInstallmentCount:installmentCount];
                                        
                                        completionHandler(orderForm, nil);
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                            }
                            else completionHandler(nil, @"http error");
                        }];
                    }
                    else completionHandler(nil, @"error creating json object");
                }
                else completionHandler(nil, @"no installment option match the criteria");
            }
            else completionHandler(nil, @"order form missing installment information");
        }
        else completionHandler(nil, @"order form missing payment information");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) addPaymentSystem:(NSString *) paymentSystem withInstallmentCount:(int) installmentCount andGiftCard:(NSDictionary*) giftcard toOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler{
    
    if(paymentSystem && orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/attachments/paymentData", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        NSMutableArray *payments = [[NSMutableArray alloc] init];
        if([orderForm paymentData]) {
            
            PaymentData *paymentData = [orderForm paymentData];
            if([paymentData installmentOptions]) {
                
                NSArray *installmentOptions = [paymentData installmentOptions];
                for(InstallmentOption *installmentOption in installmentOptions) {
                    
                    if([installmentOption paymentSystem]) {
                        if([[installmentOption paymentSystem] isEqualToString:paymentSystem]) {
                            
                            if([installmentOption installments]) {
                                
                                NSArray *installments = [installmentOption installments];
                                for(Installment *installment in installments) {
                                    
                                    if([installment count]) {
                                        if([installment count] == installmentCount) {
                                            
                                            NSDictionary *paymentDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                               [installmentOption paymentSystem], @"paymentSystem",
                                                                               [NSNumber numberWithInt:[installment count]], @"installments",
                                                                               [NSNumber numberWithLong:[installment value]], @"referenceValue",
                                                                               [NSNumber numberWithLong:[installment value]], @"value", nil];
                                            
                                            [payments addObject:paymentDictionary];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if([payments count] == 1) {
                    
                    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                           payments, @"payments",
                                                           expectedOrderFormSections, @"expectedOrderFormSections", nil];
                    
                    if(giftcard){
                        NSArray* giftCards = [[NSArray alloc] initWithObjects:giftcard, nil];
                        [bodyDictionary addEntriesFromDictionary:@{@"giftCards": giftCards}];
                    }
                    
                    NSData *bodyData;
                    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response && data) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                    
                                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    
                                    if(orderForm) {
                                        
                                        [OrderFormCheckoutStorage savePaymentSystem:paymentSystem];
                                        [OrderFormCheckoutStorage saveInstallmentCount:installmentCount];
                                        
                                        completionHandler(orderForm, nil);
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                            }
                            else completionHandler(nil, @"http error");
                        }];
                    }
                    else completionHandler(nil, @"error creating json object");
                }
                else completionHandler(nil, @"no installment option match the criteria");
            }
            else completionHandler(nil, @"order form missing installment information");
        }
        else completionHandler(nil, @"order form missing payment information");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) addTransactionToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    if(orderForm) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orderForm/%@/transaction", [self accountName], [orderForm orderFormId]];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        NSArray *expectedOrderFormSections = [[NSArray alloc] initWithObjects:
                                              @"items",
                                              @"totalizers",
                                              @"clientProfileData",
                                              @"shippingData",
                                              @"paymentData",
                                              @"sellers",
                                              @"messages",
                                              @"marketingData",
                                              @"clientPreferencesData",
                                              @"storePreferencesData",
                                              @"giftRegistryData",
                                              @"ratesAndBenefitsData",
                                              @"openTextField",
                                              @"commercialConditionData",
                                              @"customData", nil];
        
        if([orderForm orderFormId] && [orderForm paymentData]) {
            
            PaymentData *paymentData = [orderForm paymentData];
            if([paymentData payments]) {
                
                NSArray *payments = [paymentData payments];
                if([payments count] > 0) {
                    
                    long value = 0;
                    for(Payment *payment in payments){
                        value+= payment.value;
                    }
                    
                    NSMutableDictionary *bodyDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                           [orderForm orderFormId], @"referenceId",
                                                           [NSNumber numberWithLong:value], @"value",
                                                           [NSNumber numberWithLong:value], @"referenceValue",
                                                           expectedOrderFormSections, @"expectedOrderFormSections",
                                                           
                                                           nil];
                    
                    NSData *bodyData;
                    if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                        
                        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            if(!error && response && data) {
                                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                                    
                                    OrderForm *orderForm = [[OrderForm alloc] initWithJsonString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                                    
                                    if(orderForm) completionHandler(orderForm, nil);
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                            }
                            else completionHandler(nil, @"http error");
                        }];
                    }
                    else completionHandler(nil, @"error creating json object");
                }
                else completionHandler(nil, @"the order form has no payments");
            }
            else completionHandler(nil, @"order form missing payments information");
        }
        else completionHandler(nil, @"order form missing payment information");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) doGatewayCallbackWithOrderGroup:(NSString *) orderGroup andCompletionHandler:(void (^)(NSString *error)) completionHandler {
    
    if(orderGroup) {
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/gatewayCallback/%@", [self accountName], orderGroup];
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"application/json", @"accept",
                                        @"application/json", @"content-type", nil];
        
        if([self loginCookie]) [headers setObject:[self loginCookie] forKey:@"Cookie"];
        
        [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:nil andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 204) completionHandler(nil);
                else completionHandler([[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(@"http error");
        }];
    }
    else completionHandler(@"parameters cannot be null");
}

- (void) addItemsFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSArray *items;
    if([OrderFormCheckoutStorage getItems:&items]) {
        
        if(items){
            
            if([items count] > 0) [self addItems:items toOrderForm:orderForm withCompletionHandler:completionHandler];
            else completionHandler(nil, @"no items saved");
        }
        else completionHandler(nil, @"no items saved");
    }
    else completionHandler(nil, @"no items saved");
}

- (void) sendUserEmailFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSString *email;
    if([OrderFormCheckoutStorage getUserEmail:&email]) {
        
        if(email) [self sendUserEmail:email toOrderForm:orderForm withCompletionHandler:completionHandler];
        else completionHandler(nil, @"no email saved");
    }
    else completionHandler(nil, @"no email saved");
}

- (void) sendUserProfileDataFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    UserProfile *userProfile;
    if([OrderFormCheckoutStorage getUserProfileData:&userProfile]) {
        
        if(userProfile) [self sendUserProfileData:userProfile toOrderForm:orderForm withCompletionHandler:completionHandler];
        else completionHandler(nil, @"no user profile saved");
    }
    else completionHandler(nil, @"no user profile saved");
}

- (void) sendUserAddressFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    UserAddress *userAddress;
    if([OrderFormCheckoutStorage getUserAddress:&userAddress]) {
        
        if(userAddress) [self sendUserAddress:userAddress toOrderForm:orderForm withCompletionHandler:completionHandler];
        else completionHandler(nil, @"no user address saved");
    }
    else completionHandler(nil, @"no user address saved");
}

- (void) selectSlaFromStorageFromOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSString *slaId;
    NSString *deliveryChannel;
    DeliveryWindow *deliveryWindow;
    
    if([OrderFormCheckoutStorage getSlaId:&slaId]) {
        if(slaId) {
            
            if([OrderFormCheckoutStorage getDeliveryChannel:&deliveryChannel]) {
                if(deliveryChannel) {
                    
                    if([OrderFormCheckoutStorage getDeliveryWindow:&deliveryWindow]) {
                        
                        if(deliveryWindow) [self selectSlaWithId:slaId deliveryChannel:deliveryChannel andDeliveryWindow:deliveryWindow fromOrderForm:orderForm withCompletionHandler:completionHandler];
                        else [self selectSlaWithId:slaId andDeliveryChannel:deliveryChannel fromOrderForm:orderForm withCompletionHandler:completionHandler];
                    }
                    else [self selectSlaWithId:slaId andDeliveryChannel:deliveryChannel fromOrderForm:orderForm withCompletionHandler:completionHandler];
                }
                else completionHandler(nil, @"no delivery channel saved");
            }
            else completionHandler(nil, @"no delivery channel saved");
        }
        else completionHandler(nil, @"no sla id saved");
    }
    else completionHandler(nil, @"no sla id saved");
}

- (void) addPaymentSystemFromStorageToOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(OrderForm *orderForm, NSString *error)) completionHandler {
    
    NSString *paymentSystem;
    int installmentCount;
    
    if([OrderFormCheckoutStorage getPaymentSystem:&paymentSystem]) {
        if(paymentSystem) {
            
            if([OrderFormCheckoutStorage getInstallmentCount:&installmentCount]) {
                
                if(installmentCount > 0) [self addPaymentSystem:paymentSystem withInstallmentCount:installmentCount andGiftCard:nil toOrderForm:orderForm withCompletionHandler:completionHandler];
                else completionHandler(nil, @"no installment count saved");
            }
            else completionHandler(nil, @"no installment count saved");
        }
        else completionHandler(nil, @"no payment system saved");
    }
    else completionHandler(nil, @"no payment system saved");
}

@end
