//
//  Sla.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sla : NSObject

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *deliveryChannel;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *shippingEstimate;
@property (strong, nonatomic) NSString *shippingEstimateDate;
@property (strong, nonatomic) NSString *lockTTL;
@property (nonatomic) long price;
@property (nonatomic) long listPrice;
@property (nonatomic) long tax;
@property (strong, nonatomic) NSMutableArray *availableDeliveryWindows;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
