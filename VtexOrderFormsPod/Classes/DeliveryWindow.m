//
//  DeliveryWindow.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "DeliveryWindow.h"
#import "CheckoutJsonHelper.h"

@implementation DeliveryWindow

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.startDateUtc = [jsonDictionary objectForKey:@"startDateUtc"];
        self.endDateUtc = [jsonDictionary objectForKey:@"endDateUtc"];
        self.price = [[jsonDictionary objectForKey:@"price"] longValue];
        self.lisPrice = [[jsonDictionary objectForKey:@"lisPrice"] longValue];
        self.tax = [[jsonDictionary objectForKey:@"tax"] longValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       [NSNumber numberWithLong:[self price]], @"price",
                                       [NSNumber numberWithLong:[self lisPrice]], @"lisPrice",
                                       [NSNumber numberWithLong:[self tax]], @"tax", nil];
    
    if([self startDateUtc]) {
        if(![[self startDateUtc] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self startDateUtc] forKey:@"startDateUtc"];
        }
    }
    
    if([self endDateUtc]) {
        if(![[self endDateUtc] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self endDateUtc] forKey:@"endDateUtc"];
        }
    }
    
    return dictionary;
}

@end
