//
//  CreditCard.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCard : NSObject

@property (strong, nonatomic) NSString *holderName;
@property (strong, nonatomic) NSString *cardNumber;
@property (strong, nonatomic) NSString *validationCode;
@property (strong, nonatomic) NSString *dueDate;
@property (strong, nonatomic) NSString *addressId;

- (id) initWithHolderName:(NSString *) holderName cardNumber:(NSString *) cardNumber validationCode:(NSString *) validationCode dueDate:(NSString *) dueDate andAddressId:(NSString *) addressId;
- (id) initWithDictionary:(NSDictionary *) dictionary;
- (NSDictionary *) toJsonObject;

@end
