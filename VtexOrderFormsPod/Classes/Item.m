//
//  Item.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Item.h"
#import "CheckoutJsonHelper.h"

@implementation Item

- (id) initWithId:(NSString *) id quantity:(int) quantity andSeller:(NSString *) seller {
    
    if(self = [super init]) {
        
        self.id = id;
        self.quantity = quantity;
        self.seller = seller;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.name = [jsonDictionary objectForKey:@"name"];
        self.skuName = [jsonDictionary objectForKey:@"skuName"];
        self.tax = [[jsonDictionary objectForKey:@"tax"] longValue];
        self.price = [[jsonDictionary objectForKey:@"price"] longValue];
        self.listPrice = [[jsonDictionary objectForKey:@"listPrice"] longValue];
        self.sellingPrice = [[jsonDictionary objectForKey:@"sellingPrice"] longValue];
        self.isGift = [[jsonDictionary objectForKey:@"isGift"] boolValue];
        self.quantity = [[jsonDictionary objectForKey:@"quantity"] intValue];
        self.seller = [jsonDictionary objectForKey:@"seller"];
        self.availability = [jsonDictionary objectForKey:@"availability"];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       [NSNumber numberWithLong:[self tax]], @"tax",
                                       [NSNumber numberWithLong:[self price]], @"price",
                                       [NSNumber numberWithLong:[self listPrice]], @"listPrice",
                                       [NSNumber numberWithLong:[self sellingPrice]], @"sellingPrice",
                                       [NSNumber numberWithBool:[self isGift]], @"isGift",
                                       [NSNumber numberWithInt:[self quantity]], @"quantity", nil];
    
    if([self id]) {
        if(![[self id] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self id] forKey:@"id"];
        }
    }
    
    if([self name]) {
        if(![[self name] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self name] forKey:@"name"];
        }
    }
    
    if([self skuName]) {
        if(![[self skuName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self skuName] forKey:@"skuName"];
        }
    }
    
    if([self seller]) {
        if(![[self seller] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self seller] forKey:@"seller"];
        }
    }
    
    if([self availability]) {
        if(![[self availability] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self availability] forKey:@"availability"];
        }
    }
    
    return dictionary;
}

@end
