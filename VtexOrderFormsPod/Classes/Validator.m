//
//  Validator.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Validator.h"
#import "CheckoutJsonHelper.h"

@implementation Validator

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.regex = [jsonDictionary objectForKey:@"regex"];
        self.mask = [jsonDictionary objectForKey:@"mask"];
        self.cardCodeRegex = [jsonDictionary objectForKey:@"cardCodeRegex"];
        self.cardCodeMask = [jsonDictionary objectForKey:@"cardCodeMask"];
        self.useCvv = [[jsonDictionary objectForKey:@"useCvv"] boolValue];
        self.useExpirationDate = [[jsonDictionary objectForKey:@"useExpirationDate"] boolValue];
        self.useCardHolderName = [[jsonDictionary objectForKey:@"useCardHolderName"] boolValue];
        self.useBillingAddress = [[jsonDictionary objectForKey:@"useBillingAddress"] boolValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
