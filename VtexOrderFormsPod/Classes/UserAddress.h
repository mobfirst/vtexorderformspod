//
//  UserAddress.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAddress : NSObject

@property (strong, nonatomic) NSString *id;
@property (strong, nonatomic) NSString *addressType;
@property (strong, nonatomic) NSString *receiverName;
@property (strong, nonatomic) NSString *addressId;
@property (strong, nonatomic) NSString *postalCode;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *neighborhood;
@property (strong, nonatomic) NSString *complement;
@property (strong, nonatomic) NSString *reference;
@property (strong, nonatomic) NSString *jsonString;
@property (strong, nonatomic) NSString *addressName;
@property (strong, nonatomic) NSString *userId;

- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference;
- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference;
- (id) initWithAddressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement reference:(NSString *) reference addressName:(NSString *)addressName andUserId:(NSString *)userId;
- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference;
- (id) initWithId:(NSString *) id addressType:(NSString *) addressType receiverName:(NSString *) receiverName addressId:(NSString *) addressId postalCode:(NSString *) postalCode city:(NSString *) city state:(NSString *) state country:(NSString *) country street:(NSString *) street number:(NSString *) number neighborhood:(NSString *) neighborhood complement:(NSString *) complement andReference:(NSString *) reference;
- (id) initWithJsonString:(NSString *) jsonString;
- (NSDictionary *) toJsonObject;

@end
