//
//  CheckoutJsonHelper.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "CheckoutJsonHelper.h"

@implementation CheckoutJsonHelper

+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonString:(NSString *) jsonString {
    
    NSError *error;
    
    if(!jsonString) return NO;
    *dictionary = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if(!dictionary || error) return NO;
    return YES;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary withJsonKey:(NSString *) key {
    
    if(!dictionary) return NO;
    if([dictionary objectForKey:key]) {
        
        NSError *error;
        NSObject *object = [dictionary objectForKey:key];
        
        if([object isKindOfClass:[NSNull class]]) return NO;
        *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
        
        if(!jsonData || error) return NO;
        return YES;
    }
    
    return NO;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromArray:(NSArray *) array {
    
    NSError *error;
    
    if(!array) return NO;
    *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData || error) return NO;
    return YES;
}

+ (BOOL) getJsonData:(NSData **) jsonData fromDictionary:(NSDictionary *) dictionary {
    
    NSError *error;
    
    if(!dictionary) return NO;
    *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData || error) return NO;
    return YES;
}

+ (BOOL) getJsonArray:(NSArray **) jsonArray fromJsonData:(NSData *) jsonData {
    
    NSError *error;
    
    if(!jsonData) return NO;
    *jsonArray = [NSJSONSerialization JSONObjectWithData: jsonData options:kNilOptions error:&error];
    
    if(!jsonArray || error) return NO;
    return YES;
}

+ (BOOL) getDictionary:(NSDictionary **) dictionary fromJsonData:(NSData *) jsonData {
    
    NSError *error;
    
    if(!jsonData) return NO;
    *dictionary = [NSJSONSerialization JSONObjectWithData: jsonData options:kNilOptions error:&error];
    
    if(!dictionary || error) return NO;
    return YES;
}

@end
