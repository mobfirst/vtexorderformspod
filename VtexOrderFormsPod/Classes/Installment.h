//
//  Installment.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 04/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Installment : NSObject

@property (nonatomic) int count;
@property (nonatomic) BOOL hasInterestRate;
@property (nonatomic) long interestRate;
@property (nonatomic) long value;
@property (nonatomic) long total;
@property (strong, nonatomic) NSString *jsonString;

- (id) initWithJsonString:(NSString *) jsonString;

@end
