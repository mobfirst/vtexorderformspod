//
//  PaymentApi.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderForm.h"
#import "CreditCard.h"

@interface PaymentApi : NSObject

@property (nonatomic, strong) NSString *accountName;

- (id) initWithAccountName:(NSString *) accountName;
- (void) placePaymentIntoOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(NSString *orderGroup, NSString *error)) completionHandler;
- (void) placePaymentIntoOrderForm:(OrderForm *) orderForm withCreditCard:(CreditCard *) creditCard andCompletionHandler:(void (^)(NSString *orderGroup, NSString *error)) completionHandler;

@end
