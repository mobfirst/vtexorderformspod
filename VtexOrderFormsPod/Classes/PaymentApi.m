//
//  PaymentApi.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "PaymentApi.h"
#import "Payment.h"
#import "Item.h"
#import "CheckoutJsonHelper.h"
#import "HttpCheckoutConnection.h"

@implementation PaymentApi

- (id) initWithAccountName:(NSString *) accountName {
    
    if(self = [super init]) {
        self.accountName = accountName;
    }
    
    return self;
}

- (void) placePaymentIntoOrderForm:(OrderForm *) orderForm withCompletionHandler:(void (^)(NSString *orderGroup, NSString *error)) completionHandler {
    
    if(orderForm) {
        if([orderForm id] && [orderForm gatewayCallbackTemplatePath]) {
            
            NSString *url = [NSString stringWithFormat:@"https://%@.vtexpayments.com.br/api/pub/transactions/%@/payments", [self accountName], [orderForm id]];
            NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"application/json", @"accept",
                                     @"application/json", @"content-type", nil];
            
            __block NSString *orderGroup = [[[orderForm gatewayCallbackTemplatePath] stringByReplacingOccurrencesOfString:@"/checkout/gatewayCallback/" withString:@""] stringByReplacingOccurrencesOfString:@"/{messageCode}" withString:@""];
            
            if([orderForm paymentData]) {
                
                PaymentData *paymentData = [orderForm paymentData];
                if([paymentData payments]) {
                    
                    NSArray *payments = [paymentData payments];
                    if([payments count] > 0) {
                        
                        Payment *payment = [payments objectAtIndex:0];
                        if([payment paymentSystem]) {
                            
                            NSString *paymentSystem = [payment paymentSystem];
                            if([orderForm merchantTransactions]) {
                                
                                NSMutableArray *bodyArray = [[NSMutableArray alloc] init];
                                NSArray *merchantTransactions = [orderForm merchantTransactions];
                                
                                for(MerchantTransaction *merchantTransaction in merchantTransactions) {
                                    if([[merchantTransaction payments] count] > 0) {
                                        
                                        Payment *merchantPayment = [[merchantTransaction payments] objectAtIndex:0];
                                        
                                        NSDictionary *transactionDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                               [merchantTransaction transactionId], @"id",
                                                                               [merchantTransaction merchantName], @"merchantName", nil];
                                        
                                        NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                                                  [merchantTransaction id], @"id",
                                                                                  paymentSystem, @"paymentSystem",
                                                                                  [NSNumber numberWithInteger:[payment installments]], @"installments",
                                                                                  [NSNumber numberWithLong:[merchantPayment value]], @"value",
                                                                                  [NSNumber numberWithLong:[merchantPayment value]], @"referenceValue",
                                                                                  transactionDictionary, @"transaction", nil];
                                        
                                        [bodyArray addObject:paymentDictionary];
                                    }
                                }
                                
                                NSData *bodyData;
                                if([CheckoutJsonHelper getJsonData:&bodyData fromArray:bodyArray]) {
                                    [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                        
                                        if(!error && response && data) {
                                            
                                            if([((NSHTTPURLResponse *) response) statusCode] == 201) completionHandler(orderGroup, nil);
                                            else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                                        }
                                        else completionHandler(nil, @"http error");
                                    }];
                                }
                                else completionHandler(nil, @"error creating json object");
                            }
                            else completionHandler(nil, @"the order form has no transactions");
                        }
                        else completionHandler(nil, @"payment missing id");
                    }
                    else completionHandler(nil, @"the order form has no payments");
                }
                else completionHandler(nil, @"order form missing payments information");
            }
            else completionHandler(nil, @"order form missing payment information");
        }
        else completionHandler(nil, @"order form missing transaction");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

- (void) placePaymentIntoOrderForm:(OrderForm *) orderForm withCreditCard:(CreditCard *) creditCard andCompletionHandler:(void (^)(NSString *orderGroup, NSString *error)) completionHandler {
    
    if(orderForm && creditCard) {
        if([orderForm id] && [orderForm gatewayCallbackTemplatePath]) {
            
            NSString *url = [NSString stringWithFormat:@"https://%@.vtexpayments.com.br/api/pub/transactions/%@/payments", [self accountName], [orderForm id]];
            NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"application/json", @"accept",
                                     @"application/json", @"content-type", nil];
            
            __block NSString *orderGroup = [[[orderForm gatewayCallbackTemplatePath] stringByReplacingOccurrencesOfString:@"/checkout/gatewayCallback/" withString:@""] stringByReplacingOccurrencesOfString:@"/{messageCode}" withString:@""];
            
            if([orderForm paymentData]) {
                
                PaymentData *paymentData = [orderForm paymentData];
                if([paymentData payments]) {
                    
                    NSArray *payments = [paymentData payments];
                    if([payments count] > 0) {
                        
                        Payment *payment = [payments objectAtIndex:0];
                        if([payment paymentSystem]) {
                            
                            NSString *paymentSystem = [payment paymentSystem];
                            if([orderForm merchantTransactions]) {
                                
                                NSMutableArray *bodyArray = [[NSMutableArray alloc] init];
                                NSArray *merchantTransactions = [orderForm merchantTransactions];
                                
                                for(MerchantTransaction *merchantTransaction in merchantTransactions) {
                                    if([[merchantTransaction payments] count] > 0) {
                                        
                                        Payment *merchantPayment = [[merchantTransaction payments] objectAtIndex:0];
                                        
                                        NSDictionary *transactionDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                               [merchantTransaction transactionId], @"id",
                                                                               [merchantTransaction merchantName], @"merchantName", nil];
                                        
                                        NSMutableDictionary *paymentDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                                                  [merchantTransaction id], @"id",
                                                                                  paymentSystem, @"paymentSystem",
                                                                                  [NSNumber numberWithInteger:[payment installments]], @"installments",
                                                                                  [NSNumber numberWithLong:[merchantPayment value]], @"value",
                                                                                  [NSNumber numberWithLong:[merchantPayment value]], @"referenceValue",
                                                                                  [creditCard toJsonObject], @"fields",
                                                                                  transactionDictionary, @"transaction", nil];
                                        
                                        [bodyArray addObject:paymentDictionary];
                                    }
                                }
                                
                                NSData *bodyData;
                                if([CheckoutJsonHelper getJsonData:&bodyData fromArray:bodyArray]) {
                                    [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                        
                                        if(!error && response && data) {
                                            
                                            if([((NSHTTPURLResponse *) response) statusCode] == 201) completionHandler(orderGroup, nil);
                                            else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                                        }
                                        else completionHandler(nil, @"http error");
                                    }];
                                }
                                else completionHandler(nil, @"error creating json object");
                            }
                            else completionHandler(nil, @"the order form has no transactions");
                        }
                        else completionHandler(nil, @"payment missing id");
                    }
                    else completionHandler(nil, @"the order form has no payments");
                }
                else completionHandler(nil, @"order form missing payments information");
            }
            else completionHandler(nil, @"order form missing payment information");
        }
        else completionHandler(nil, @"order form missing transaction");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

@end
