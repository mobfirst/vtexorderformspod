//
//  OrderForm.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "OrderForm.h"
#import "CheckoutJsonHelper.h"
#import "Item.h"
#import "Message.h"
#import "Totalizer.h"

@implementation OrderForm

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *itemsData;
        if([CheckoutJsonHelper getJsonData:&itemsData fromDictionary:jsonDictionary withJsonKey:@"items"]) {
            
            NSArray *itemsArray;
            if([CheckoutJsonHelper getJsonArray:&itemsArray fromJsonData:itemsData]) {
                
                self.items = [[NSMutableArray alloc] init];
                for(NSDictionary *itemDictionary in itemsArray) {
                    
                    NSData *itemData;
                    if(![CheckoutJsonHelper getJsonData:&itemData fromDictionary:itemDictionary]) continue;
                    
                    [self.items addObject:[[Item alloc] initWithJsonString:[[NSString alloc] initWithData:itemData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *messagesData;
        if([CheckoutJsonHelper getJsonData:&messagesData fromDictionary:jsonDictionary withJsonKey:@"messages"]) {
            
            NSArray *messagesArray;
            if([CheckoutJsonHelper getJsonArray:&messagesArray fromJsonData:messagesData]) {
                
                self.messages = [[NSMutableArray alloc] init];
                for(NSDictionary *messageDictionary in messagesArray) {
                    
                    NSData *messageData;
                    if(![CheckoutJsonHelper getJsonData:&messageData fromDictionary:messageDictionary]) continue;
                    
                    [self.messages addObject:[[Message alloc] initWithJsonString:[[NSString alloc] initWithData:messageData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *totalizersData;
        if([CheckoutJsonHelper getJsonData:&totalizersData fromDictionary:jsonDictionary withJsonKey:@"totalizers"]) {
            
            NSArray *totalizersArray;
            if([CheckoutJsonHelper getJsonArray:&totalizersArray fromJsonData:totalizersData]) {
                
                self.totalizers = [[NSMutableArray alloc] init];
                for(NSDictionary *totalizerDictionary in totalizersArray) {
                    
                    NSData *totalizerData;
                    if(![CheckoutJsonHelper getJsonData:&totalizerData fromDictionary:totalizerDictionary]) continue;
                    
                    [self.totalizers addObject:[[Totalizer alloc] initWithJsonString:[[NSString alloc] initWithData:totalizerData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *merchantTransactionsData;
        if([CheckoutJsonHelper getJsonData:&merchantTransactionsData fromDictionary:jsonDictionary withJsonKey:@"merchantTransactions"]) {
            
            NSArray *merchantTransactionsArray;
            if([CheckoutJsonHelper getJsonArray:&merchantTransactionsArray fromJsonData:merchantTransactionsData]) {
                
                self.merchantTransactions = [[NSMutableArray alloc] init];
                for(NSDictionary *merchantTransactionDictionary in merchantTransactionsArray) {
                    
                    NSData *merchantTransactionData;
                    if(![CheckoutJsonHelper getJsonData:&merchantTransactionData fromDictionary:merchantTransactionDictionary]) continue;
                    
                    [self.merchantTransactions addObject:[[MerchantTransaction alloc] initWithJsonString:[[NSString alloc] initWithData:merchantTransactionData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *userProfileData;
        if([CheckoutJsonHelper getJsonData:&userProfileData fromDictionary:jsonDictionary withJsonKey:@"clientProfileData"]) {
            self.userProfile = [[UserProfile alloc] initWithJsonString:[[NSString alloc] initWithData:userProfileData encoding:NSUTF8StringEncoding]];
        }
        
        NSData *shippingDataData;
        if([CheckoutJsonHelper getJsonData:&shippingDataData fromDictionary:jsonDictionary withJsonKey:@"shippingData"]) {
            self.shippingData = [[ShippingData alloc] initWithJsonString:[[NSString alloc] initWithData:shippingDataData encoding:NSUTF8StringEncoding]];
        }
        
        NSData *paymentDataData;
        if([CheckoutJsonHelper getJsonData:&paymentDataData fromDictionary:jsonDictionary withJsonKey:@"paymentData"]) {
            self.paymentData = [[PaymentData alloc] initWithJsonString:[[NSString alloc] initWithData:paymentDataData encoding:NSUTF8StringEncoding]];
        }
        
        self.orderFormId = [jsonDictionary objectForKey:@"orderFormId"];
        self.orderId = [jsonDictionary objectForKey:@"orderId"];
        self.userProfileId = [jsonDictionary objectForKey:@"userProfileId"];
        self.id = [jsonDictionary objectForKey:@"id"];
        self.gatewayCallbackTemplatePath = [jsonDictionary objectForKey:@"gatewayCallbackTemplatePath"];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
