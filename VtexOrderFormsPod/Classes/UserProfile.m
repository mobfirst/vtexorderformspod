//
//  UserProfile.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "UserProfile.h"
#import "CheckoutJsonHelper.h"

@implementation UserProfile

- (id) initWithFirstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate andStateInscription:(NSString *) stateInscription {
    
    if(self = [super init]) {
        
        self.firstEmail = firstEmail;
        self.email = email;
        self.firstName = firstName;
        self.lastName = lastName;
        self.document = document;
        self.phone = phone;
        self.documentType = documentType;
        self.isCorporate = isCorporate;
        self.stateInscription = stateInscription;
    }
    
    return self;
}

- (id) initWithFirstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate stateInscription:(NSString *) stateInscription attachmentId:(NSString *) attachmentId corporateName:(NSString *) corporateName tradeName:(NSString *) tradeName corporateDocument:(NSString *) corporateDocument corporatePhone:(NSString *) corporatePhone profileCompleteOnLoading:(BOOL) profileCompleteOnLoading profileErrorOnLoading:(BOOL) profileErrorOnLoading andCustomerClass:(NSString *) customerClass {
    
    if(self = [super init]) {
        
        self.firstEmail = firstEmail;
        self.email = email;
        self.firstName = firstName;
        self.lastName = lastName;
        self.document = document;
        self.phone = phone;
        self.documentType = documentType;
        self.isCorporate = isCorporate;
        self.stateInscription = stateInscription;
        self.attachmentId = attachmentId;
        self.corporateName = corporateName;
        self.tradeName = tradeName;
        self.corporateDocument = corporateDocument;
        self.corporatePhone = corporatePhone;
        self.profileCompleteOnLoading = profileCompleteOnLoading;
        self.profileErrorOnLoading = profileErrorOnLoading;
        self.customerClass = customerClass;
    }
    
    return self;
}

- (id) initWithId:(NSString *) id firstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate andStateInscription:(NSString *) stateInscription {
    
    if(self = [super init]) {
        
        self.id = id;
        self.firstEmail = firstEmail;
        self.email = email;
        self.firstName = firstName;
        self.lastName = lastName;
        self.document = document;
        self.phone = phone;
        self.documentType = documentType;
        self.isCorporate = isCorporate;
        self.stateInscription = stateInscription;
    }
    
    return self;
}
- (id) initWithId:(NSString *) id firstEmail:(NSString *) firstEmail email:(NSString *) email firstName:(NSString *) firstName lastName:(NSString *) lastName document:(NSString *) document phone:(NSString *) phone documentType:(NSString *) documentType isCorporate:(BOOL) isCorporate stateInscription:(NSString *) stateInscription attachmentId:(NSString *) attachmentId corporateName:(NSString *) corporateName tradeName:(NSString *) tradeName corporateDocument:(NSString *) corporateDocument corporatePhone:(NSString *) corporatePhone profileCompleteOnLoading:(BOOL) profileCompleteOnLoading profileErrorOnLoading:(BOOL) profileErrorOnLoading andCustomerClass:(NSString *) customerClass {
    
    if(self = [super init]) {
        
        self.id = id;
        self.firstEmail = firstEmail;
        self.email = email;
        self.firstName = firstName;
        self.lastName = lastName;
        self.document = document;
        self.phone = phone;
        self.documentType = documentType;
        self.isCorporate = isCorporate;
        self.stateInscription = stateInscription;
        self.attachmentId = attachmentId;
        self.corporateName = corporateName;
        self.tradeName = tradeName;
        self.corporateDocument = corporateDocument;
        self.corporatePhone = corporatePhone;
        self.profileCompleteOnLoading = profileCompleteOnLoading;
        self.profileErrorOnLoading = profileErrorOnLoading;
        self.customerClass = customerClass;
    }
    
    return self;
}

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.firstEmail = [jsonDictionary objectForKey:@"firstEmail"];
        self.email = [jsonDictionary objectForKey:@"email"];
        self.firstName = [jsonDictionary objectForKey:@"firstName"];
        self.lastName = [jsonDictionary objectForKey:@"lastName"];
        self.document = [jsonDictionary objectForKey:@"document"];
        self.phone = [jsonDictionary objectForKey:@"phone"];
        self.documentType = [jsonDictionary objectForKey:@"documentType"];
        self.isCorporate = [[jsonDictionary objectForKey:@"isCorporate"] boolValue];
        self.stateInscription = [jsonDictionary objectForKey:@"stateInscription"];
        self.attachmentId = [jsonDictionary objectForKey:@"attachmentId"];
        self.corporateName = [jsonDictionary objectForKey:@"corporateName"];
        self.tradeName = [jsonDictionary objectForKey:@"tradeName"];
        self.corporateDocument = [jsonDictionary objectForKey:@"corporateDocument"];
        self.corporatePhone = [jsonDictionary objectForKey:@"corporatePhone"];
        self.customerClass = [jsonDictionary objectForKey:@"customerClass"];
        
        if([jsonDictionary objectForKey:@"profileCompleteOnLoading"] && ![[jsonDictionary objectForKey:@"profileCompleteOnLoading"] isKindOfClass:[NSNull class]]) {
            self.profileCompleteOnLoading = [[jsonDictionary objectForKey:@"profileCompleteOnLoading"] boolValue];
        }
        if([jsonDictionary objectForKey:@"profileErrorOnLoading"] && ![[jsonDictionary objectForKey:@"profileErrorOnLoading"] isKindOfClass:[NSNull class]]) {
            self.profileErrorOnLoading = [[jsonDictionary objectForKey:@"profileErrorOnLoading"] boolValue];
        }
        
        self.jsonString = jsonString;
    }
    
    return self;
}

- (NSDictionary *) toJsonObject {
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
    if([self id]) {
        if(![[self id] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self id] forKey:@"id"];
        }
    }
    
    if([self firstEmail]) {
        if(![[self firstEmail] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self firstEmail] forKey:@"firstEmail"];
        }
    }
    
    if([self email]) {
        if(![[self email] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self email] forKey:@"email"];
        }
    }
    
    if([self firstName]) {
        if(![[self firstName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self firstName] forKey:@"firstName"];
        }
    }
    
    if([self lastName]) {
        if(![[self lastName] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self lastName] forKey:@"lastName"];
        }
    }
    
    if([self document]) {
        if(![[self document] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self document] forKey:@"document"];
        }
    }
    
    if([self phone]) {
        if(![[self phone] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self phone] forKey:@"phone"];
        }
    }
    
    if([self documentType]) {
        if(![[self documentType] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self documentType] forKey:@"documentType"];
        }
    }
    
    if([self isCorporate]) [dictionary setObject:[NSNumber numberWithBool:[self isCorporate]] forKey:@"isCorporate"];
    
    if([self stateInscription]) {
        if(![[self stateInscription] isKindOfClass:[NSNull class]]) {
            [dictionary setObject:[self stateInscription] forKey:@"stateInscription"];
        }
    }
    
    return dictionary;
}

@end
