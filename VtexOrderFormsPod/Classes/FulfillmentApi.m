//
//  FulfillmentApi.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 05/06/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "FulfillmentApi.h"
#import "Item.h"
#import "CheckoutJsonHelper.h"
#import "HttpCheckoutConnection.h"
#import "LogisticsInfo.h"

@implementation FulfillmentApi

- (id)initWithAccountName:(NSString *)accountName apiKey:(NSString *)apiKey andApiToken:(NSString *)apiToken {
    if(self = [super init]) {
        
        self.accountName = accountName;
        self.apiKey = apiKey;
        self.apiToken = apiToken;
    }
    
    return self;
}

- (void)simulateShippingWithItems:(NSArray *)items postalCode:(NSString *)postalCode country:(NSString *)country andCompletionHandler:(void (^)(NSArray *logisticsInfo, NSString *error)) completionHandler {
    
    if(items && postalCode && country) {
        if([items count] > 0) {
            
            NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/fulfillment/pvt/orderForms/simulation", [self accountName]];
            NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                            @"application/json", @"accept",
                                            @"application/json", @"content-type",
                                            [self apiToken], @"X-VTEX-API-AppToken",
                                            [self apiKey], @"X-VTEX-API-AppKey", nil];
            
            NSMutableArray *orderItems = [[NSMutableArray alloc] init];
            
            for(Item *item in items) {
                [orderItems addObject:[item toJsonObject]];
            }
            
            NSDictionary *bodyDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                            postalCode, @"postalCode",
                                            country, @"country",
                                            orderItems, @"items", nil];
            
            NSData *bodyData;
            if([CheckoutJsonHelper getJsonData:&bodyData fromDictionary:bodyDictionary]) {
                
                [HttpCheckoutConnection doPostToUrl:url withHeaders:headers parameters:nil body:bodyData andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(!error && response && data) {
                        if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                            
                            NSDictionary *responseDictionary;
                            if([CheckoutJsonHelper getDictionary:&responseDictionary fromJsonData:data]) {
                                
                                NSData *logisticsInfoData;
                                if([CheckoutJsonHelper getJsonData:&logisticsInfoData fromDictionary:responseDictionary withJsonKey:@"logisticsInfo"]) {
                                    
                                    NSArray *logisticsInfoArray;
                                    if([CheckoutJsonHelper getJsonArray:&logisticsInfoArray fromJsonData:logisticsInfoData]) {
                                        
                                        NSMutableArray *logisticsInfo = [[NSMutableArray alloc] init];
                                        for(NSDictionary *logisticsItemDictionary in logisticsInfoArray) {
                                            
                                            NSData *logisticsItemData;
                                            if(![CheckoutJsonHelper getJsonData:&logisticsItemData fromDictionary:logisticsItemDictionary]) continue;
                                            
                                            [logisticsInfo addObject:[[LogisticsInfo alloc] initWithJsonString:[[NSString alloc] initWithData:logisticsItemData encoding:NSUTF8StringEncoding]]];
                                        }
                                        
                                        completionHandler(logisticsInfo, nil);
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, @"error parsing response");
                            }
                            else completionHandler(nil, @"error parsing response");
                        }
                        else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    }
                    else completionHandler(nil, @"http error");
                }];
            }
            else completionHandler(nil, @"error creating json object");
        }
        else completionHandler(nil, @"the array has no items");
    }
    else completionHandler(nil, @"parameters cannot be null");
}

@end
