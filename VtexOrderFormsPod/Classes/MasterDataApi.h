//
//  MasterdataApi.h
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 25/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "UserAddress.h"

@interface MasterDataApi : NSObject

@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *vtexApiKey;
@property (nonatomic, strong) NSString *vtexApiToken;

- (id) initWithAccountName:(NSString *) acoountName vtexApiKey:(NSString *) vtexApiKey andVtexApiToken:(NSString *) vtexApiToken;

- (void) getUserProfileWithUserId:(NSString *) userId andCompletionHandler:(void (^)(UserProfile *userProfile, NSString *error)) completionHandler;
- (void) updateUserProfile:(UserProfile *) oldUserProfile withNewUserProfile:(UserProfile *) newUserProfile andCompletionHandler:(void (^)(NSString *error)) completionHandler;
- (void) getUserAddressesFromUserProfile:(UserProfile *) userProfile andCompletionHandler:(void (^)(NSArray *addresses, NSString *error)) completionHandler;
- (void) updateUserAddress:(UserAddress *) oldUserAddress withNewUserAddress:(UserAddress *) newUserAddress andCompletionHandler:(void (^)(NSString *error)) completionHandler;
- (void) createUserAddressWithUserProfile:(UserProfile *) userProfile userAddress:(UserAddress *) userAddress andCompletionHandler:(void (^)(NSString *error)) completionHandler;

@end
