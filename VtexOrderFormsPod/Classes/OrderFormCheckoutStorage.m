//
//  OrderFormCheckoutStorage.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 09/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "OrderFormCheckoutStorage.h"
#import "CheckoutJsonHelper.h"
#import "Item.h"

@implementation OrderFormCheckoutStorage

+ (BOOL) getItems:(NSArray **) items {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSArray *jsonArray = [defaults arrayForKey:@"OrderFormCheckoutStorageItems"];
        if(jsonArray) {
            
            NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
            for(NSDictionary *itemDictionary in jsonArray) {
                
                NSData *itemData;
                if(![CheckoutJsonHelper getJsonData:&itemData fromDictionary:itemDictionary]) continue;
                
                [itemsArray addObject:[[Item alloc] initWithJsonString:[[NSString alloc] initWithData:itemData encoding:NSUTF8StringEncoding]]];
            }
            
            *items = [[NSArray alloc] initWithArray:itemsArray];
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) getUserEmail:(NSString **) email {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *email = [defaults stringForKey:@"OrderFormCheckoutStorageUserEmail"];
        if(email) return YES;
    }
    
    return NO;
}

+ (BOOL) getUserProfileData:(UserProfile **) userProfile {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:@"OrderFormCheckoutStorageUserProfile"];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([CheckoutJsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *userProfile = [[UserProfile alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(userProfile) return YES;
            }
        }
    }
    
    return NO;
}

+ (BOOL) getUserAddress:(UserAddress **) userAddress {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:@"OrderFormCheckoutStorageUserAddress"];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([CheckoutJsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *userAddress = [[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(userAddress) return YES;
            }
        }
    }
    
    return NO;
}

+ (BOOL) getSlaId:(NSString **) slaId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *slaId = [defaults stringForKey:@"OrderFormCheckoutStorageSlaId"];
        if(slaId) return YES;
    }
    
    return NO;
}

+ (BOOL) getDeliveryChannel:(NSString **) deliveryChannel {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *deliveryChannel = [defaults stringForKey:@"OrderFormCheckoutStorageDeliveryChannel"];
        if(deliveryChannel) return YES;
    }
    
    return NO;
}

+ (BOOL) getDeliveryWindow:(DeliveryWindow **) deliveryWindow {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        NSDictionary *jsonDictionary = [defaults dictionaryForKey:@"OrderFormCheckoutStorageDeliveryWindow"];
        if(jsonDictionary) {
            
            NSData *jsonData;
            if([CheckoutJsonHelper getJsonData:&jsonData fromDictionary:jsonDictionary]) {
                
                *deliveryWindow = [[DeliveryWindow alloc] initWithJsonString:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
                if(deliveryWindow) return YES;
            }
        }
    }
    
    return NO;
}

+ (BOOL) getPaymentSystem:(NSString **) paymentSystem {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *paymentSystem = [defaults stringForKey:@"OrderFormCheckoutStoragePaymentSystem"];
        if(paymentSystem) return YES;
    }
    
    return NO;
}

+ (BOOL) getInstallmentCount:(int *) installmentCount {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        *installmentCount = (int) [defaults integerForKey:@"OrderFormCheckoutStorageInstallmentCount"];
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveItems:(NSArray *) items {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && items) {
        
        NSMutableArray *jsonArray = [[NSMutableArray alloc] init];
        for(Item *item in items) {
            
            NSDictionary *itemDictionary = [item toJsonObject];
            
            if(itemDictionary) {
                [jsonArray addObject:[item toJsonObject]];
            }
        }
        
        [defaults setObject:jsonArray forKey:@"OrderFormCheckoutStorageItems"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveUserEmail:(NSString *) email {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && email) {
        
        [defaults setObject:email forKey:@"OrderFormCheckoutStorageUserEmail"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveUserProfileData:(UserProfile *) userProfile {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && userProfile) {
        
        NSDictionary *userProfileDictionary = [userProfile toJsonObject];
        if(userProfileDictionary) {
            
            [defaults setObject:userProfileDictionary forKey:@"OrderFormCheckoutStorageUserProfile"];
            [defaults synchronize];
            
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) saveUserAddress:(UserAddress *) userAddress {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && userAddress) {
        
        NSDictionary *userAddressDictionary = [userAddress toJsonObject];
        if(userAddressDictionary) {
            
            [defaults setObject:userAddressDictionary forKey:@"OrderFormCheckoutStorageUserAddress"];
            [defaults synchronize];
            
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) saveSlaId:(NSString *) slaId {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && slaId) {
        
        [defaults setObject:slaId forKey:@"OrderFormCheckoutStorageSlaId"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveDeliveryChannel:(NSString *) deliveryChannel {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && deliveryChannel) {
        
        [defaults setObject:deliveryChannel forKey:@"OrderFormCheckoutStorageDeliveryChannel"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveDeliveryWindow:(DeliveryWindow *) deliveryWindow {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && deliveryWindow) {
        
        NSDictionary *deliveryWindowDictionary = [deliveryWindow toJsonObject];
        if(deliveryWindowDictionary) {
            
            [defaults setObject:deliveryWindowDictionary forKey:@"OrderFormCheckoutStorageDeliveryWindow"];
            [defaults synchronize];
            
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL) savePaymentSystem:(NSString *) paymentSystem {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults && paymentSystem) {
        
        [defaults setObject:paymentSystem forKey:@"OrderFormCheckoutStoragePaymentSystem"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) saveInstallmentCount:(int) installmentCount {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        [defaults setInteger:installmentCount forKey:@"OrderFormCheckoutStorageInstallmentCount"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

+ (BOOL) deleteDeliveryWindow {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults) {
        
        [defaults removeObjectForKey:@"OrderFormCheckoutStorageDeliveryWindow"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}

@end
