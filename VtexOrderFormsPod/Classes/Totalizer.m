//
//  Totalizer.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Totalizer.h"
#import "CheckoutJsonHelper.h"

@implementation Totalizer

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.name = [jsonDictionary objectForKey:@"name"];
        self.value = [[jsonDictionary objectForKey:@"value"] longValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
