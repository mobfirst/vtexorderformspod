//
//  OmsApi.m
//  CheckoutLibrary
//
//  Created by Diego Merks on 16/11/17.
//  Copyright © 2017 Diego Merks. All rights reserved.
//

#import "OmsApi.h"
#import "HttpCheckoutConnection.h"
#import "CheckoutJsonHelper.h"

@implementation OmsApi

- (id) initWithAccountName:(NSString *) accountName andLoginCookie:(NSString *) loginCookie {
    
    if(self = [super init]) {
        
        self.accountName = accountName;
        self.loginCookie = loginCookie;
    }
    
    return self;
}

- (void) getBankInvoiceFromOrderGroup:(NSString *) orderGroup withCompletionHandler:(void (^)(NSString *invoiceUrl, NSString *error)) completionHandler {
    
    if(orderGroup) {
        
        NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"application/json", @"accept",
                                 @"application/json", @"content-type",
                                 [self loginCookie], @"Cookie", nil];
        
        NSString *url = [NSString stringWithFormat:@"https://%@.vtexcommercestable.com.br/api/checkout/pub/orders/order-group/%@",
                              [self accountName], orderGroup];
        
        [HttpCheckoutConnection doGetFromUrl:url withHeaders:headers parameters:nil andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && response && data) {
                if([((NSHTTPURLResponse *) response) statusCode] == 200) {
                    
                    NSArray *jsonArray;
                    if([CheckoutJsonHelper getJsonArray:&jsonArray fromJsonData:data]) {
                        
                        if([jsonArray count] > 0) {
                            NSDictionary *jsonDictionary = [jsonArray objectAtIndex:0];
                            
                            NSDictionary *paymentDataObject = [jsonDictionary objectForKey:@"paymentData"];
                            NSArray *transactionsArray = [paymentDataObject objectForKey:@"transactions"];
                            
                            if([transactionsArray count] > 0) {
                                
                                NSDictionary *transactionObject = [transactionsArray objectAtIndex:0];
                                NSArray *paymentsArray = [transactionObject objectForKey:@"payments"];
                                
                                if(paymentsArray) {
                                    
                                    if([paymentsArray count] > 0) {
                                        
                                        NSDictionary *paymentDictionary = [paymentsArray objectAtIndex:0];
                                        if(paymentDictionary) {
                                            
                                            NSString *invoiceUrl = [paymentDictionary objectForKey:@"url"];
                                            
                                            if(invoiceUrl) completionHandler(invoiceUrl, nil);
                                            else completionHandler(nil, @"error parsing response");
                                        }
                                        else completionHandler(nil, @"error parsing response");
                                    }
                                    else completionHandler(nil, @"error parsing response");
                                }
                                else completionHandler(nil, @"error parsing response");
                            }
                            else completionHandler(nil, @"error parsing response");
                        }
                        else completionHandler(nil, @"error parsing response");
                    }
                    else completionHandler(nil, @"error parsing response");
                }
                else completionHandler(nil, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
            else completionHandler(nil, @"http error");
        }];
    }
    else completionHandler(nil, @"parameters cannot be null");
}

@end
