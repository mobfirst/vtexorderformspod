//
//  Sla.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "Sla.h"
#import "CheckoutJsonHelper.h"
#import "DeliveryWindow.h"

@implementation Sla

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *availableDeliveryWindowsData;
        if([CheckoutJsonHelper getJsonData:&availableDeliveryWindowsData fromDictionary:jsonDictionary withJsonKey:@"availableDeliveryWindows"]) {
            
            NSArray *availableDeliveryWindowsArray;
            if([CheckoutJsonHelper getJsonArray:&availableDeliveryWindowsArray fromJsonData:availableDeliveryWindowsData]) {
                
                self.availableDeliveryWindows = [[NSMutableArray alloc] init];
                for(NSDictionary *availableDeliveryWindowDictionary in availableDeliveryWindowsArray) {
                    
                    NSData *availableDeliveryWindowData;
                    if(![CheckoutJsonHelper getJsonData:&availableDeliveryWindowData fromDictionary:availableDeliveryWindowDictionary]) continue;
                    
                    [self.availableDeliveryWindows addObject:[[DeliveryWindow alloc] initWithJsonString:[[NSString alloc] initWithData:availableDeliveryWindowData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        self.id = [jsonDictionary objectForKey:@"id"];
        self.deliveryChannel = [jsonDictionary objectForKey:@"deliveryChannel"];
        self.name = [jsonDictionary objectForKey:@"name"];
        self.shippingEstimate = [jsonDictionary objectForKey:@"shippingEstimate"];
        self.shippingEstimateDate = [jsonDictionary objectForKey:@"shippingEstimateDate"];
        self.lockTTL = [jsonDictionary objectForKey:@"lockTTL"];
        self.price = [[jsonDictionary objectForKey:@"price"] longValue];
        self.listPrice = [[jsonDictionary objectForKey:@"listPrice"] longValue];
        self.tax = [[jsonDictionary objectForKey:@"tax"] longValue];
        
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
