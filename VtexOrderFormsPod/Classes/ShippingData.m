//
//  ShippingData.m
//  VtexCheckoutWithOrderForms
//
//  Created by Diego Merks on 03/04/18.
//  Copyright © 2018 Mobfirst. All rights reserved.
//

#import "ShippingData.h"
#import "CheckoutJsonHelper.h"
#import "LogisticsInfo.h"

@implementation ShippingData

- (id) initWithJsonString:(NSString *) jsonString {
    
    if(self = [super init]) {
        
        NSDictionary *jsonDictionary;
        if(![CheckoutJsonHelper getDictionary:&jsonDictionary fromJsonString:jsonString]) return self;
        
        NSData *availableAddressesData;
        if([CheckoutJsonHelper getJsonData:&availableAddressesData fromDictionary:jsonDictionary withJsonKey:@"availableAddresses"]) {
            
            NSArray *availableAddressesArray;
            if([CheckoutJsonHelper getJsonArray:&availableAddressesArray fromJsonData:availableAddressesData]) {
                
                self.availableAddresses = [[NSMutableArray alloc] init];
                for(NSDictionary *availableAddress in availableAddressesArray) {
                    
                    NSData *availableAddressData;
                    if(![CheckoutJsonHelper getJsonData:&availableAddressData fromDictionary:availableAddress]) continue;
                    
                    [self.availableAddresses addObject:[[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:availableAddressData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *selectedAddressesData;
        if([CheckoutJsonHelper getJsonData:&selectedAddressesData fromDictionary:jsonDictionary withJsonKey:@"selectedAddresses"]) {
            
            NSArray *selectedAddressesArray;
            if([CheckoutJsonHelper getJsonArray:&selectedAddressesArray fromJsonData:selectedAddressesData]) {
                
                self.selectedAddresses = [[NSMutableArray alloc] init];
                for(NSDictionary *selectedAddress in selectedAddressesArray) {
                    
                    NSData *selectedAddressData;
                    if(![CheckoutJsonHelper getJsonData:&selectedAddressData fromDictionary:selectedAddress]) continue;
                    
                    [self.selectedAddresses addObject:[[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:selectedAddressData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *logisticsInfoData;
        if([CheckoutJsonHelper getJsonData:&logisticsInfoData fromDictionary:jsonDictionary withJsonKey:@"logisticsInfo"]) {
            
            NSArray *logisticsInfoArray;
            if([CheckoutJsonHelper getJsonArray:&logisticsInfoArray fromJsonData:logisticsInfoData]) {
                
                self.logisticsInfo = [[NSMutableArray alloc] init];
                for(NSDictionary *logisticsItemDictionary in logisticsInfoArray) {
                    
                    NSData *logisticsItemData;
                    if(![CheckoutJsonHelper getJsonData:&logisticsItemData fromDictionary:logisticsItemDictionary]) continue;
                    
                    [self.logisticsInfo addObject:[[LogisticsInfo alloc] initWithJsonString:[[NSString alloc] initWithData:logisticsItemData encoding:NSUTF8StringEncoding]]];
                }
            }
        }
        
        NSData *addressData;
        if([CheckoutJsonHelper getJsonData:&addressData fromDictionary:jsonDictionary withJsonKey:@"address"]) {
            self.address = [[UserAddress alloc] initWithJsonString:[[NSString alloc] initWithData:addressData encoding:NSUTF8StringEncoding]];
        }
        
        self.attachmentId = [jsonDictionary objectForKey:@"attachmentId"];
        self.jsonString = jsonString;
    }
    
    return self;
}

@end
